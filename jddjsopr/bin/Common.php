<?php
/**
 * 异常退出
 */
function ErrorExit($msg){
	die($msg."\n");
}

/**
 * 获取数据库连接
 */
function getDbConn()
{
	$config = include '../Application/Common/Conf/config.php';
	$server = sprintf ( "%s:%d", $config ["DB_HOST"], $config ["DB_PORT"] );
	$dbconn = mysql_connect ( $server, $config ["DB_USER"], $config ["DB_PWD"] );
	if($dbconn===false){
		ErrorExit("数据库连接失败");
	}
	mysql_query ( "SET NAMES " . $config ["DB_CHARSET"] );
	mysql_select_db ( $config ["DB_NAME"], $dbconn );
	return $dbconn;
}

/**
 * 关闭数据库连接
 */
function closeDB($conn){
	mysql_close($conn);
	return true;
}

/**
 * 数据库查询
 */
function SelectDB($sql,$dbconn=null)
{
	$list=array();
	$autoclose=false;
	if($dbconn==null)
	{
		$dbconn=getDbConn();
		$autoclose=true;
	}
	$result=mysql_query($sql,$dbconn);
	while($row=mysql_fetch_array($result))
	{
		array_push($list,$row);
	}
	mysql_free_result($result);
	if($autoclose==true)
	{
		mysql_close($dbconn);
	}
	return $list;
}

/**
 * 数据库更新
 */
function UpdateDB($sql,$dbconn=null)
{
	$result=false;
	$autoclose=false;
	if($dbconn==null)
	{
		$dbconn=getDbConn();
		$autoclose=true;
	}
	$result=mysql_query($sql,$dbconn);
	if($autoclose==true)
	{
		mysql_close($dbconn);
	}
	return $result;
}
