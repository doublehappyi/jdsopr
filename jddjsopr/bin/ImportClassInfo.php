<?php
/**
 * 导入京东到家类目信息
 * 源文件需GBK编码，列字段为：classid1\tclassname1\tclassid2\tclassname2\tclassid3\tclassname3，不带列头
 * 源文件通过命令行参数传入
 */

require 'Common.php';
require '../Application/Common/Common/function.php';

//处理命令行参数
date_default_timezone_set ( "Asia/Shanghai" );
if($argc!=2){
	ErrorExit(sprintf("Usage %s filepath",$argv[0]));
}

$filehandle = fopen($argv[1], "r");
if(!$filehandle){
	ErrorExit ("文件读取失败");
}

//删除原有数据
$dbconn=getDbConn();
if(UpdateDB("delete from sopr_ClassInfo",$dbconn)===false){
	closeDB($dbconn);
	fclose($filehandle);
	ErrorExit ("清空数据失败");
}

$totalrow=0;
$validrow=0;
while(!feof($filehandle))
{
	$totalrow++;
	$strline=fgets($filehandle);
	$datalist=explode("\t",$strline);
	if(count($datalist)==6){
		if(isPositiveNumeric($datalist[0]) && isPositiveNumeric($datalist[2]) && isPositiveNumeric($datalist[4])){
			$sql=sprintf("insert into sopr_ClassInfo(classId1,className1,classId2,className2,classId3,className3) 
					values('%s','%s','%s','%s','%s','%s')",$datalist[0],mysql_escape_string(GBK2UTF8($datalist[1])),$datalist[2],
					mysql_escape_string(GBK2UTF8($datalist[3])),$datalist[4],mysql_escape_string(GBK2UTF8($datalist[5])));
			if(UpdateDB($sql,$dbconn)===false){
				closeDB($dbconn);
				fclose($filehandle);
				ErrorExit ("导入数据失败");
			}
			$validrow++;
		}
	}
}
closeDB($dbconn);
fclose($filehandle);
echo sprintf("all %d rows,import %d rows\n",$totalrow,$validrow);
exit(0);

