<?php
/**
 * 导入自动挖掘热门类目
 * 源文件需GBK编码，列字段为：keyword\tclassid\tscore;不带列头
 * 源文件通过命令行参数传入
 */

require 'Common.php';
require '../Application/Common/Common/function.php';

//处理命令行参数
date_default_timezone_set ( "Asia/Shanghai" );
if($argc!=2){
	ErrorExit(sprintf("Usage %s filepath",$argv[0]));
}

$filehandle = fopen($argv[1], "r");
if(!$filehandle){
	ErrorExit ("文件读取失败");
}

//删除原有数据
$dbconn=getDbConn();
if(UpdateDB("delete from sopr_HotClassSys",$dbconn)===false){
	closeDB($dbconn);
	fclose($filehandle);
	ErrorExit ("清空数据失败");
}

$totalrow=0;
$validrow=0;
while(!feof($filehandle))
{
	$totalrow++;
	$strline=fgets($filehandle);
	$datalist=explode("\t",$strline);
	if(count($datalist)==3){
		$keyword=mysql_escape_string(GBK2UTF8(trim($datalist[0])));
		$classid=trim($datalist[1]);
		$score=trim($datalist[2]);
		if(isValidString($keyword) && isPositiveNumeric($classid) && is_numeric($score)){
			$sql=sprintf("insert into sopr_HotClassSys(keyName,classId,classPoint,createTime) 
					values('%s','%s','%s','%s')",$keyword,$classid,$score,date('Y-m-d H:i:s'));
			if(UpdateDB($sql,$dbconn)===false){
				closeDB($dbconn);
				fclose($filehandle);
				ErrorExit ("导入数据失败");
			}
			$validrow++;
		}
	}
}
closeDB($dbconn);
fclose($filehandle);
echo sprintf("all %d rows,import %d rows\n",$totalrow,$validrow);
exit(0);

