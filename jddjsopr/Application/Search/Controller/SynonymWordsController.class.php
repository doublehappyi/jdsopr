<?php
namespace Search\Controller;
use Think\Controller;
use Common\Controller\SoprController;
use Org\Net\Http;

class SynonymWordsController extends SoprController {
	
	 private $tempfile;
	 private $pubfile;
	 private $uploadConfig;
	
	function __construct(){
		parent::__construct();
		$this->moduleKey="synonymwords";
		$this->tempfile=C('WEB_ROOT_PATH')."/sysfiles/publish/synonym.txt";
		$this->pubfile=C('PUB_DATA_PATH')."/synonym/synonym.txt";
		$this->uploadConfig= array('maxSize' => 524288,'rootPath' => './sysfiles/','subName' => 'upload','exts' => array('txt'));
	}
	
	public function index(){
		$this->display("index");
	}
	
	/**
	 * 查询同义词
	 */
    public function getSynonymWords(){
    	$keyword = trim ( I ( "keyword" ) );
    	$page = trim ( I ( "page" ) );
    	$pagesize = trim ( I ( "pagesize" ) );
    	
    	$page = isPositiveNumeric ( $page ) ? intval ( $page ) : 0;
    	$pagesize = isPositiveNumeric ( $pagesize ) ? intval ( $pagesize ) : $this::PAGESIZE_DEFAULT;
    	
    	$dao=new \Search\Model\SynonymWordsModel();
    	$data=$dao->getSynonymWords($keyword, $page, $pagesize);
    	$page=$dao->getSynonymWordsPageInfo($keyword, $page, $pagesize);
    	return $this->ajaxReturnSuccess ( $data, $page);
    }
    
    /**
     * 查询当前用户保存未发布同义词
     */
    public function getUnPubSynonymWords(){
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\SynonymWordsModel();
    	$data=$dao->getUnPubData($optUser);
    	return $this->ajaxReturnSuccess ( $data );
    }
    
    private function SaveOneSynonymWords($keyword,$avaidatetime,$optUser,$content){
    	if(!isValidString($keyword)){
    		return "关键字无效";
    	}
    	if(!isDateTimeString($avaidatetime)){
    		return "有效期无效";
    	}
    	$tmparray1=explode(':',$content);//word1:word2
    	$tmparray2=array();
    	$synonym="";
    	$num=0;
    	for($i=0;$i<count($tmparray1);$i++){
    		$tmpstr=trim($tmparray1[$i]);
    		if(!isValidString($tmpstr)){
    			continue;
    		}
    		if(!in_array($tmpstr,$tmparray2)){
    			array_push($tmparray2, $tmpstr);
    			$synonym=$synonym.$tmpstr.":";
    			$num++;
    		}
    	}
    	if($num==0){
    		return "同义词不能为空";
    	}
    	$dao = new \Search\Model\SynonymWordsModel();
    	$ret=$dao->saveSynonymWords($keyword, $avaidatetime, $synonym, $optUser);
    	if($ret===false){
    		return "保存失败".$dao->getDbError();
    	}
    	$this->writeLog($this::LOGTYPE_MODIFY,$keyword,sprintf("avaiDateTime:%s,synonym:%s",$avaidatetime,$synonym));
    	return true;
    }
    
    /**
     * 保存同义词
     */
    public  function saveSynonymWords(){
      if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$keyword = trim ( I ( "keyword" ) );
    	$avaidatetime= trim ( I ( "avaidatetime" ) );
    	if(!isValidString($avaidatetime)){
    		$avaidatetime=$this::AVAILABLE_DATETIME_MAXED;
    	}
    	$content=trim ( I ( "synonym" ) );
    	$optUser=$this->getUserFromSession("userName");
    	$ret=$this->SaveOneSynonymWords($keyword, $avaidatetime, $optUser, $content);
    	if($ret===true){
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError($ret);
    }
    
    /**
     * 删除纠错词
     */
    public function delSynonymWords(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	 
    	$keyword = trim ( I ( "keyId" ) );
    	if(!isPositiveNumeric($keyword)){
    		return $this->ajaxReturnError("输入参数无效");
    	}
    	 
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\SynonymWordsModel();
    	$ret=$dao->delSynonymWords($keyword, $optUser);
    	if($ret===true){
    		$this->writeLog($this::LOGTYPE_DELETE, "",sprintf("keyId:%d",$keyword));
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError($dao->getError());
    }
    
    /**
     * 发布同义词
     */
    public  function pubSynonymWords(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	//获取本模块信息
    	$moduleObj=$this->getModuleInfo();
    	if($moduleObj===false){
    		return $this->ajaxReturnError ( "模块不存在或已经删除");
    	}
    	
    	//更新发布状态
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\SynonymWordsModel();
    	if($dao->updatePubData($optUser)===false){
    		return $this->ajaxReturnError ( "更新发布状态失败");
    	}
    	
    	//写文件
    	$datalist=$dao->getPubData();
    	$myfile=fopen($this->tempfile,"w");
    	if($myfile===false){
    		return $this->ajaxReturnError ( "打开写文件失败");
    	}
    	$format="%s\t%s\n";
    	for($i=0;$i<count($datalist);$i++){
    		$keyword=UTF82GBK($datalist[$i]["keyName"]);
    		$synonym=str_replace(":", "\t", UTF82GBK($datalist[$i]["synonym"]));
    		$content=sprintf($format,$keyword,$synonym);
    		if(fwrite($myfile, $content)===false){
    			fclose($myfile);
    			return $this->ajaxReturnError ( "写发布文件失败");
    		}
    	}
    	fclose($myfile);
    	
    	//推送脚本
    	$output=null;
    	$errno=0;
    	$command=sprintf("cp -f %s %s;cd %s;./SynctoOperationSvr.sh sync %s",$this->tempfile,$this->pubfile,C("PUB_SHELL_PATH"),$moduleObj["syncFilePath"]);
    	$errinfo=exec($command,$output,$errno);
    	if($errno!=0){
    		return $this->ajaxReturnError ( "文件同步失败");
    	}
    	$this->writeLog($this::LOGTYPE_PUB, "", "");
    	return $this->ajaxReturnSuccess ();
    }
    
    /**
     * 导出同义词
     */
    public function exportSynonymWords(){
    	Http::download($this->tempfile);
    }
    
    /**
     * 导入同义词
     */
    public function importSynonymWords(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	$upload = new \Think\Upload($this->uploadConfig);// 实例化上传类
    	$info = $upload->uploadOne($_FILES['uploadHotClass']);//上传文件
    	if($info===false){
    		return $this->ajaxReturnError ($upload->getError());
    	}
    	$fileName=$upload->rootPath.$info['savepath'].$info['savename'];
    	if(!($filehandle = fopen($fileName, "r"))){
    		return $this->ajaxReturnError ("文件读取失败");
    	}
    	$validline=0;
    	$rowindex=0;
    	$optUser=$this->getUserFromSession("userName");
    	while(!feof($filehandle))
    	{
    		$rowindex++;
    		$strline=fgets($filehandle);
    		$datalist=explode("\t",$strline);
    		$keyword="";
    		$content="";
    		$avaiTime=$this::AVAILABLE_DATETIME_MAXED;
    		if(count($datalist)==2 || count($datalist)==3){
	    		if(count($datalist)==2)
	    		{
	    			$keyword=iconv("GBK", "UTF-8//IGNORE", trim($datalist[0]));
	    			$content=iconv("GBK", "UTF-8//IGNORE", trim($datalist[1]));
	    		}
	    		if(count($datalist)==3)
	    		{
	    			$keyword=iconv("GBK", "UTF-8//IGNORE", trim($datalist[0]));
	    			$avaiTime=trim($datalist[1]);
	    			$content=iconv("GBK", "UTF-8//IGNORE", trim($datalist[2]));
	    		}
    			$ret=$this->SaveOneSynonymWords($keyword, $avaiTime, $optUser, $content);
    			if($ret===true){
    				$validline++;
    			}else{
    				fclose($filehandle);
    				unlink($fileName);
    				return $this->ajaxReturnError(sprintf("Row%d,导入失败:%s",$rowindex,$ret));
    			}
    		}else{
    			fclose($filehandle);
    			unlink($fileName);
    			return $this->ajaxReturnError(sprintf("Row%d,导入失败:字段格式错误",$rowindex));
    		}
    	}
    	fclose($filehandle);
    	unlink($fileName);
    	return $this->ajaxReturnSuccess($validline);
    }
}