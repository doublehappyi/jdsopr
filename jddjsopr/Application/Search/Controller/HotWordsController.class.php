<?php
namespace Search\Controller;
use Think\Controller;
use Common\Controller\SoprController;
use Org\Net\Http;
use Search\Model\HotWordsModel;

class HotWordsController extends SoprController {
	
	 private $tempfile;
	 private $pubfile;
	 private $uploadConfig;
	
	function __construct(){
		parent::__construct();
		$this->moduleKey="hotwords";
		$this->tempfile=C('WEB_ROOT_PATH')."/sysfiles/publish/hot_words.txt";
		$this->pubfile=C('PUB_DATA_PATH')."/hotwords/hot_words.txt";
		$this->uploadConfig= array('maxSize' => 524288,'rootPath' => './sysfiles/','subName' => 'upload','exts' => array('txt'));
	}
	
	public function index(){
		$this->display("index");
	}
	
	private  function NcaStoreInfo($store_id){
		if(isPositiveNumeric($store_id)){
			$data=array();
			return $data;
		}
		else{
			return false;
		}
	}
    
	/**
	 * 门店ID查询门店信息
	 */
	public function getStoreInfo(){
		$store_id=trim ( I ( "store_id" ) );
		$ret=$this->NcaStoreInfo($store_id);
		if($ret===false){
			return $this->ajaxReturnError(sprintf("门店ID:%s无效",$store_id));
		}else{
			return $this->ajaxReturnSuccess($ret);
		}
	}
	
	/**
	 * 查询热词
	 */
	public function getHotWords() {
		$keyword = trim ( I ( "keyword" ) );
		$type = trim ( I ( "intervention" ) );
		$orgcode = trim ( I ( "orgcode" ) );
		$storeid = trim ( I ( "storeid" ) );
		$page = trim ( I ( "page" ) );
		$pagesize = trim ( I ( "pagesize" ) );
		if (isValidString ( $type ) && ! is_numeric ( $type )) {
			return $this->ajaxReturnError ( "请求参数热词类型无效" );
		}
		if (isValidString ( $orgcode ) && ! isPositiveNumeric ( $orgcode )) {
			return $this->ajaxReturnError ( "请求参数org_code无效" );
		}
		if (isValidString ( $storeid ) && ! isPositiveNumeric ( $storeid )) {
			return $this->ajaxReturnError ( "请求参数门店ID无效" );
		}
		$page = isPositiveNumeric ( $page ) ? intval ( $page ) : 0;
		$pagesize = isPositiveNumeric ( $pagesize ) ? intval ( $pagesize ) : $this::PAGESIZE_DEFAULT;
		
		$dao = new \Search\Model\HotWordsModel ();
		$data = $dao->getHotWords ( $type, $keyword, $orgcode, $storeid, $page, $pagesize );
		$page = $dao->getHotWordsPageInfo ( $type, $keyword, $orgcode, $storeid, $page, $pagesize );
		return $this->ajaxReturnSuccess ( $data, $page );
	}
	
	/**
	 * 查询当前用户保存未发布热词
	 */
	public function getUnPubHotWords(){
		$optUser=$this->getUserFromSession("userName");
		$dao = new \Search\Model\HotWordsModel();
		$data=$dao->getUnPubHotWords($optUser);
		return $this->ajaxReturnSuccess ( $data );
	}
	
	private function SaveOneHotWords($keyword,$content,$type,$starttime,$endtime,$optUser){
		if(!isValidString($keyword)){
			return "关键字无效";
		}
		if(!isPositiveNumeric($type)){
			return "热词类型无效";
		}
		$type=intval($type);
		if($type!=HotWordsModel::INTERVENTION_TYPE_USER && $type!=HotWordsModel::INTERVENTION_TYPE_FILTER){
			return "热词类型无效";
		}
		if($type==HotWordsModel::INTERVENTION_TYPE_USER && !isDateTimeString($starttime)){
			return "生效日期无效";
		}
		if($type==HotWordsModel::INTERVENTION_TYPE_USER && !isDateTimeString($endtime)){
			return "失效日期无效";
		}
		$storeids=explode(',',$content);
		$storeids2=array();
		for($i=0;$i<count($storeids);$i++){
			$storeid=trim($storeids[$i]);
			if($storeid==""){
				continue;
			}
			if($this->NcaStoreInfo($storeid)===false ){
				return sprintf("门店ID:%s无效",$storeid);
			}
			array_push($storeids2, $storeid);
		}
		if($type==HotWordsModel::INTERVENTION_TYPE_USER && count($storeids2)==0){
			return "门店ID不能为空";
		}
		$dao = new \Search\Model\HotWordsModel();
		$ret=false;
		if($type==HotWordsModel::INTERVENTION_TYPE_USER){
			$ret=$dao->saveUserHotWords($keyword, $storeids2, $starttime, $endtime, $optUser);
		}else{
			$ret=$dao->saveFilterHotWords($keyword, $storeids2, $optUser);
		}
		if($ret===false){
			return "保存失败".$dao->getDbError();
		}
		$this->writeLog($this::LOGTYPE_MODIFY, $keyword, sprintf("start:%s,end:%s,intervention:%d,content:%s",$starttime,$endtime,$type,$content));
		return true;
	}
	
	/**
	 * 保存热词
	 */
	public  function saveUserHotWords(){
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		 
		$keyword = trim ( I ( "keyword" ) );
		$type=trim ( I ( "intervention" ) );
		$storeids= trim ( I ( "storeids" ) );
		$starttime=trim ( I ( "starttime" ) );
		$endtime=trim ( I ( "endtime" ) );
		$optUser=$this->getUserFromSession("userName");
		$ret=$this->SaveOneHotWords($keyword, $storeids,$type,$starttime, $endtime,$optUser);
		if($ret===true){
			return $this->ajaxReturnSuccess();
		}
		return $this->ajaxReturnError($ret);
	}
	
	/**
	 * 发布热词
	 */
	public  function pubHotWords(){
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		 
		//获取本模块信息
		$moduleObj=$this->getModuleInfo();
		if($moduleObj===false){
			return $this->ajaxReturnError ( "模块不存在或已经删除");
		}
		 
		//更新发布状态
		$optUser=$this->getUserFromSession("userName");
		$dao = new \Search\Model\HotWordsModel();
		if($dao->PubHotWords($optUser)===false){
			return $this->ajaxReturnError ( "更新发布状态失败");
		}
		 
		//写文件
		$datalist=$dao->getPubHotWords();
		$myfile=fopen($this->tempfile,"w");
		if($myfile===false){
			return $this->ajaxReturnError ( "打开写文件失败");
		}
		$format="%s\t%d\t%d\t%d\t%d\t%d\n";
		for($i=0;$i<count($datalist);$i++){
			$keyword=UTF82GBK($datalist[$i]["keywords"]);
			$content=sprintf($format,$keyword,$datalist[$i]["store_id"],$datalist[$i]["sort_num"],strtotime($datalist[$i]["create_time"]),
					strtotime($datalist[$i]["startTime"]),strtotime($datalist[$i]["endTime"]));
			if(fwrite($myfile, $content)===false){
				fclose($myfile);
				return $this->ajaxReturnError ( "写发布文件失败");
			}
		}
		fclose($myfile);
		 
		//推送脚本
		$output=null;
		$errno=0;
		$command=sprintf("cp -f %s %s;cd %s;./SynctoOperationSvr.sh sync %s",$this->tempfile,
		$this->pubfile,C("PUB_SHELL_PATH"),$moduleObj["syncFilePath"]);
		$errinfo=exec($command,$output,$errno);
		if($errno!=0){
			return $this->ajaxReturnError ( "文件同步失败");
		}
		$this->writeLog($this::LOGTYPE_PUB, "", "");
		return $this->ajaxReturnSuccess ();
	}
		
		/**
		 * 导出热词
		 */
		public function exportHotWords(){
			$keyword = trim ( I ( "keyword" ) );
			$type = trim ( I ( "intervention" ) );
			$orgcode = trim ( I ( "orgcode" ) );
			$storeid = trim ( I ( "storeid" ) );
			if (isValidString ( $type ) && ! is_numeric ( $type )) {
				return $this->ajaxReturnError ( "请求参数热词类型无效" );
			}
			if (isValidString ( $orgcode ) && ! isPositiveNumeric ( $orgcode )) {
				return $this->ajaxReturnError ( "请求参数org_code无效" );
			}
			if (isValidString ( $storeid ) && ! isPositiveNumeric ( $storeid )) {
				return $this->ajaxReturnError ( "请求参数门店ID无效" );
			}
			$page = 0;
			$pagesize = isPositiveNumeric ( $pagesize ) ? intval ( $pagesize ) : $this::PAGESIZE_MAXED;
			
			$dao = new \Search\Model\HotWordsModel ();
			$datalist = $dao->getHotWords ( $type, $keyword, $orgcode, $storeid, $page, $pagesize );
			//组装excel
			if(Vendor('phpExcel.PHPExcel')===false){
 				return $this->returnPopMsgPage("加载PHPExcel库失败");
 			}
	 		$objExcel = new \PHPExcel();
	 		$objExcel->setActiveSheetIndex(0);
	 		$objExcel->getActiveSheet()->setCellValue('A1', "热词");
	 		$objExcel->getActiveSheet()->setCellValue('B1', "门店ID");
	 		$objExcel->getActiveSheet()->setCellValue('C1', "热词类型");
	 		$objExcel->getActiveSheet()->setCellValue('D1', "排序");
	 		$objExcel->getActiveSheet()->setCellValue('E1', "生效时间");
	 		$objExcel->getActiveSheet()->setCellValue('F1', "失效时间");
	 		
	 		for($i=0;$i<count($datalist); $i++) {
	 			$row=strval($i+2);
	 			$objExcel->getActiveSheet()->setCellValue('A'.$row, $datalist[$i]["keywords"]);
	 			$objExcel->getActiveSheet()->setCellValue('B'.$row, $datalist[$i]["store_id"]);
	 			$objExcel->getActiveSheet()->setCellValue('C'.$row, $this->tranintervention($datalist[$i]["intervention"]));
	 			$objExcel->getActiveSheet()->setCellValue('D'.$row, $datalist[$i]["sort_num"]);
	 			$objExcel->getActiveSheet()->setCellValue('E'.$row, $datalist[$i]["startTime"]);
	 			$objExcel->getActiveSheet()->setCellValue('F'.$row, $datalist[$i]["endTime"]);
	 		}
	 		
	 		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	 		header('Content-Disposition: attachment;filename="labelexport.xlsx"');
	 		header('Cache-Control: max-age=0');
	 		$objWriter = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel2007');
	 		$objWriter->save('php://output');
	 		exit(0);
		}
		
		private function tranintervention($type){
			$type=intval($type);
			switch($type){
				case 0:return "系统挖掘热词";
				case 1:return "人工热词";
				case 2:return "屏蔽热词";
				default: return "未知";
			}
		}
		
		/**
		 * 删除热词
		 */
		public function delHotWords(){
			if($this->checkUserRight()==false){
				return $this->ajaxReturnError ( "没有操作权限");
			}
			 
			$ids=trim(I("ids"));
			$optUser=$this->getUserFromSession("userName");
			$idlist=explode(",", $ids);
			for($i=0;$i<count($idlist);$i++){
				if(!isPositiveNumeric($idlist[$i])){
					return $this->ajaxReturnError ( "输入参数无效");
				}
			}
			$dao = new \Search\Model\HotWordsModel();
			if($dao->delHotWords($idlist, $optUser)===false){
				return $this->ajaxReturnError("删除失败".$dao->getDbError());
			}
			$this->writeLog($this::LOGTYPE_DELETE, "", sprintf("ids:%s",$ids));
			return $this->ajaxReturnSuccess();
		}
		
		/**
		 * 屏蔽热词
		 */
		public function filterHotWords(){
			if($this->checkUserRight()==false){
				return $this->ajaxReturnError ( "没有操作权限");
			}
		
			$ids=trim(I("ids"));
			$optUser=$this->getUserFromSession("userName");
			$idlist=explode(",", $ids);
			for($i=0;$i<count($idlist);$i++){
				if(!isPositiveNumeric($idlist[$i])){
					return $this->ajaxReturnError ( "输入参数无效");
				}
			}
			$dao = new \Search\Model\HotWordsModel();
			if($dao->filterHotWords($idlist, $optUser)===false){
				return $this->ajaxReturnError("屏蔽失败".$dao->getDbError());
			}
			$this->writeLog($this::LOGTYPE_MODIFY, "", sprintf("intervention:%d,ids:%s",HotWordsModel::INTERVENTION_TYPE_FILTER,$ids));
			return $this->ajaxReturnSuccess();
		}
		
		/**
		 * 修改排序
		 */
		public function changeSortNum(){
			if($this->checkUserRight()==false){
				return $this->ajaxReturnError ( "没有操作权限");
			}
		
			$id=trim(I("id"));
			$sortnum=trim(I("sortnum"));
			$optUser=$this->getUserFromSession("userName");
			 
			if(!isPositiveNumeric($id)){
				return $this->ajaxReturnError ( "输入参数ID无效");
			}
			if(!isPositiveNumeric($sortnum)){
				return $this->ajaxReturnError ( "输入参数顺序号无效");
			}
			 
			$dao = new \Search\Model\HotWordsModel();
			if($dao->changeSortNum($id, $sortnum, $optUser)===false){
				return $this->ajaxReturnError("排序失败".$dao->getDbError());
			}
			$this->writeLog($this::LOGTYPE_MODIFY, "", sprintf("id:%d,sort_num:%s",$id,$sortnum));
			return $this->ajaxReturnSuccess();
		}
}