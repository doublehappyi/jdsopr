<?php

namespace Search\Controller;

use Think\Controller;
use Common\Controller\SoprController;
use Think\Model;
use Org\Net\Http;

class DirtyWordsController extends SoprController {
	private $tempDataFile;//老业务用，只包含脏词（不含屏蔽指定品类/召回指定品类的词）
	private $tempDataFileVsb;//smartbox用，只包含脏词（含屏蔽指定品类/召回指定品类的词）
	private $tempDataFileV2;//运营平台脏词用，脏词 匹配方式 有效期（不含屏蔽指定品类/召回指定品类的词）
	private $tempDataFileV3;//运营平台脏词用，脏词 匹配方式 应用类型 有效期 品类ID（只含屏蔽指定品类/召回指定品类的词）
	private $pubDataFile;
	private $pubDataFileVsb;
	private $pubDataFileV2;
	private $pubDataFileV3;
	private $uploadConfig;
	private $tempExportFile;
	
	function __construct() {
		parent::__construct ();
		$this->moduleKey = "dirtywords";
		$this->tempDataFile = C ( "WEB_ROOT_PATH" ) . "/sysfiles/publish/ForbiddenWords.txt";
		$this->tempDataFileVsb = C ( "WEB_ROOT_PATH" ) . "/sysfiles/publish/ForbiddenWords_smartbox.txt";
		$this->tempDataFileV2 = C ( "WEB_ROOT_PATH" ) . "/sysfiles/publish/ForbiddenWords_v2.txt";
		$this->tempDataFileV3 = C ( "WEB_ROOT_PATH" ) . "/sysfiles/publish/ForbiddenWords_v3.txt";
		$this->pubDataFile = C ( "PUB_DATA_PATH" ) . "/dirtyword/ForbiddenWords.txt";
		$this->pubDataFileVsb = C ( "PUB_DATA_PATH" ) . "/dirtyword/ForbiddenWords_smartbox.txt";
		$this->pubDataFileV2 = C ( "PUB_DATA_PATH" ) . "/dirtyword/ForbiddenWords_v2.txt";
		$this->pubDataFileV3 = C ( "PUB_DATA_PATH" ) . "/dirtyword/ForbiddenWords_v3.txt";
		$this->tempExportFile = C ( "WEB_ROOT_PATH" ) . "/sysfiles/tempfile/ForbiddenWords.txt";
		$this->uploadConfig = array ('maxSize' => 524288,'rootPath' => './tempfiles/','subName' => 'upload','exts' => array ('txt'));
	}
	
	public function index() {
		$this->assign ( "defaultAvaiTime", $this::AVAILABLE_DATETIME_MAXED );
		$this->display ( "index" );
	}
	
	public function getDirtyWords() {
		$pkId = trim ( I ( "pkId" ) );
		$keyword = trim ( I ( "keyword" ) );
		$islike = (trim ( I ( "islike" ) ) == "1") ? true : false;
		$expType = trim ( I ( "expType" ) );
		$useType = trim ( I ( "useType" ) );
		$page = trim ( I ( "page" ) );
		$pagesize = trim ( I ( "pagesize" ) );
		
		if (isNoValidString ( $keyword )) {
			$keyword = "";
		}
		$pkId = isPositiveNumeric ( $pkId ) ? intval ( $pkId ) : - 1;
		$expType = isPositiveNumeric ( $expType ) ? intval ( $expType ) : - 1;
		$useType = isPositiveNumeric ( $useType ) ? intval ( $useType ) : - 1;
		$page = isPositiveNumeric ( $page ) ? intval ( $page ) : 0;
		$pagesize = isPositiveNumeric ( $pagesize ) ? intval ( $pagesize ) : $this::PAGESIZE_DEFAULT;
		
		$dao = new \Search\Model\DirtyWordsModel ();
		$pageinfo = $dao->getDirtyWordsPageInfo ( $pkId, $keyword, $expType, $useType, $islike, $page, $pagesize );
		$list = $dao->getDirtyWords ( $pkId, $keyword, $expType, $useType, $islike, $page, $pagesize );
		$this->ajaxReturnSuccess ( $list, $pageinfo );
	}
	
	public function saveDirtyWords() {
		if ($this->checkUserRight () == false) {
			return $this->ajaxReturnError ( "没有操作权限" );
		}
		
		$pkId = trim ( I ( "pkId" ) );
		if ($pkId == "") {
			$pkId = "-1";
		}
		$keyWord = trim ( I ( "keyWord" ) );
		$avaiTime = trim ( I ( "avaiTime" ) );
		$expType = trim ( I ( "expType" ) );
		$useType = trim ( I ( "useType" ) );
		$classids = trim ( I ( "classids" ) );
		$classids = $this->deleteRepeatClassid ( $classids );
		$checkret = $this->checkParam ( $keyWord, $avaiTime, $expType, $useType, $classids, $pkId );
		if ($checkret ["result"] != true) {
			return $this->ajaxReturnError ( $checkret ["errmsg"] );
		}
		$optUser = $this->getUserFromSession ( "userName" );
		$expType = intval ( $expType );
		$useType = intval ( $useType );
		$pkId = intval ( $pkId );
		$classids = $useType == 0 ? "" : $classids;
		if ($pkId > 0) {
			// 修改
			$dao = new \Search\Model\DirtyWordsModel ();
			if ($dao->saveDirtyWords ( $pkId, $keyWord, $avaiTime, $expType, $useType, $classids, $optUser ) === false) {
				return $this->ajaxReturnError ( "修改失败" );
			} else {
				$log = sprintf ( "pkId:%d,keyWord:%s,avaiTime:%s,expType:%d,useType:%d,classids:%s", $pkId, $keyWord, $avaiTime, $expType, $useType, $classids );
				$this->writeLog ( $this::LOGTYPE_MODIFY, $keyWord, $log );
				return $this->ajaxReturnSuccess ();
			}
		} else {
			// 新增
			$dao = new \Search\Model\DirtyWordsModel ();
			if ($dao->addDirtyWords ( $keyWord, $avaiTime, $expType, $useType, $classids, $optUser ) === false) {
				return $this->ajaxReturnError ( "新增失败" );
			} else {
				$log = sprintf ( "keyWord:%s,avaiTime:%s,expType:%d,useType:%d,classids:%s", $keyWord, $avaiTime, $expType, $useType, $classids );
				$this->writeLog ( $this::LOGTYPE_NEW, $keyWord, $log );
				return $this->ajaxReturnSuccess ();
			}
		}
	}
	
	public function delDirtyWords() {
		if ($this->checkUserRight () == false) {
			return $this->ajaxReturnError ( "没有操作权限" );
		}
		$pkId = trim ( I ( "pkId" ) );
		$keyWord = trim ( I ( "keyWord" ) );
		$optUser = $this->getUserFromSession ( "userName" );
		if (isPositiveNumeric ( $pkId )) {
			$pkId = intval ( $pkId );
			$dao = new \Search\Model\DirtyWordsModel ();
			if ($dao->delDirtyWords ( $pkId, $optUser ) === false) {
				return $this->ajaxReturnError ( "删除失败" );
			} else {
				$log = sprintf ( "pkId:%d", $pkId );
				$this->writeLog ( $this::LOGTYPE_DELETE, $keyWord, $log );
				return $this->ajaxReturnSuccess ();
			}
		} else {
			return $this->ajaxReturnError ( "输入参数无效" );
		}
	}
	
	public function exportDirtyWords() {
		$myfile = fopen ( $this->tempExportFile, "w" );
		if ($myfile === false) {
			return;
		}
		$dao = new \Search\Model\DirtyWordsModel ();
		$datalist = $dao->getPubData ();
		$len = count ( $datalist );
		
		$format = "%s\t%d\t%d\t%s\t%s\n"; // keyword exptype usetype avaitime classids
		for($i = 0; $i < $len; $i ++) {
			$keyWord = UTF82GBK ( $datalist [$i] ['keyWord'] );
			$content = sprintf ( $format, $keyWord, $datalist [$i] ['expType'], $datalist [$i] ['useType'], $datalist [$i] ['avaiTime'], $datalist [$i] ['classids'] );
			if (fwrite ( $myfile, $content ) === false) {
				fclose ( $myfile );
				return;
			}
		}
		fclose ( $myfile );
		Http::download ( $this->tempExportFile );
	}
	
	public function importDirtyWords() {
		if ($this->checkUserRight () == false) {
			echo sprintf ( '<script type="text/javascript">window.parent.showUploadResult(\'%s\');</script>', "没有操作权限" );
			return;
		}
		$upload = new \Think\Upload ( $this->uploadConfig ); // 实例化上传类
		$info = $upload->uploadOne ( $_FILES ['uploadDirtyWords'] ); // 上传文件
		if ($info === false) {
			echo sprintf ( '<script type="text/javascript">window.parent.showUploadResult(\'%s\');</script>', $upload->getError () );
			return;
		}
		$fileName = $upload->rootPath . $info ['savepath'] . $info ['savename'];
		if (! ($filehandle = fopen ( $fileName, "r" ))) {
			echo sprintf ( '<script type="text/javascript">window.parent.showUploadResult(\'%s\');</script>', "文件读取失败" );
			return;
		}
		$validline = 0;
		$rowindex = 0;
		$dao = new \Search\Model\DirtyWordsModel ();
		$optUser = $this->getUserFromSession ( "userName" );
		$errinfo = "";
		while ( ! feof ( $filehandle ) ) {
			$strline = fgets ( $filehandle );
			$rowindex ++;
			if (! isValidString ( $strline )) {
				continue;
			}
			$datalist = explode ( "\t", $strline );
			if (count ( $datalist ) >= 5) {
				// keyword exptype usetype avaitime classids
				$keyWord = GBK2UTF8 ( trim ( $datalist [0] ) );
				$avaiTime = trim ( $datalist [3] );
				$expType = trim ( $datalist [1] );
				$useType = trim ( $datalist [2] );
				$classids = $this->deleteRepeatClassid ( trim ( $datalist [4] ) );
				$checkret = $this->checkParam ( $keyWord, $avaiTime, $expType, $useType, $classids, "-1" );
				if ($checkret ["result"] != true) {
					$errinfo = $errinfo . sprintf ( "Row[%d]:%s\n", $rowindex, $checkret ["errmsg"] );
					continue;
				}
				$expType = intval ( $expType );
				$useType = intval ( $useType );
				$classids = $useType == 0 ? "" : $classids;
				if ($dao->addDirtyWords ( $keyWord, $avaiTime, $expType, $useType, $classids, $optUser )) {
					$log = sprintf ( "keyWord:%s,avaiTime:%s,expType:%d,useType:%d,classids:%s", $keyWord, $avaiTime, $expType, $useType, $classids );
					$this->writeLog ( $this::LOGTYPE_NEW, $keyWord, $log );
					$validline ++;
				} else {
					$errinfo = $errinfo . sprintf ( "Row[%d] Err:%s\n", $rowindex, $dao->getError () );
				}
			} else {
				$errinfo = $errinfo . sprintf ( "Row[%d]:%s\n", $rowindex, "列格式错误" );
			}
		}
		fclose ( $filehandle );
		unlink ( $fileName );
		echo "<script type=\"text/javascript\">window.parent.showUploadResult(\"" . "成功导入" . strval ( $validline ) . "行数据\\n" . str_replace ( "\n", "\\n", $errinfo ) . "\");</script>";
	}
	
	public function importDirtyWordsNew() {
		if ($this->checkUserRight () == false) {
			return $this->ajaxReturnError("没有操作权限");
		}
		$upload = new \Think\Upload ( $this->uploadConfig ); // 实例化上传类
		$info = $upload->uploadOne ( $_FILES ['uploadDirtyWords'] ); // 上传文件
		if ($info === false) {
			return $this->ajaxReturnError($upload->getError ());
		}
		$fileName = $upload->rootPath . $info ['savepath'] . $info ['savename'];
		if (! ($filehandle = fopen ( $fileName, "r" ))) {
			return $this->ajaxReturnError("文件读取失败");
		}
		$validline = 0;
		$rowindex = 0;
		$dao = new \Search\Model\DirtyWordsModel ();
		$optUser = $this->getUserFromSession ( "userName" );
		$errinfo = "";
		while ( ! feof ( $filehandle ) ) {
			$strline = fgets ( $filehandle );
			$rowindex ++;
			if (! isValidString ( $strline )) {
				continue;
			}
			$datalist = explode ( "\t", $strline );
			if (count ( $datalist ) >= 5) {
				// keyword exptype usetype avaitime classids
				$keyWord = GBK2UTF8 ( trim ( $datalist [0] ) );
				$avaiTime = trim ( $datalist [3] );
				$expType = trim ( $datalist [1] );
				$useType = trim ( $datalist [2] );
				$classids = $this->deleteRepeatClassid ( trim ( $datalist [4] ) );
				$checkret = $this->checkParam ( $keyWord, $avaiTime, $expType, $useType, $classids, "-1" );
				if ($checkret ["result"] != true) {
					$errinfo = $errinfo . sprintf ( "Row[%d]:%s\n", $rowindex, $checkret ["errmsg"] );
					continue;
				}
				$expType = intval ( $expType );
				$useType = intval ( $useType );
				$classids = $useType == 0 ? "" : $classids;
				if ($dao->addDirtyWords ( $keyWord, $avaiTime, $expType, $useType, $classids, $optUser )) {
					$log = sprintf ( "keyWord:%s,avaiTime:%s,expType:%d,useType:%d,classids:%s", $keyWord, $avaiTime, $expType, $useType, $classids );
					$this->writeLog ( $this::LOGTYPE_NEW, $keyWord, $log );
					$validline ++;
				} else {
					$errinfo = $errinfo . sprintf ( "Row[%d] Err:%s\n", $rowindex, $dao->getError () );
				}
			} else {
				$errinfo = $errinfo . sprintf ( "Row[%d]:%s\n", $rowindex, "列格式错误" );
			}
		}
		fclose ( $filehandle );
		unlink ( $fileName );
		return $this->ajaxReturnSuccess("成功导入" . strval ( $validline ) . "行数据," .$errinfo );
	}
	
	
	
	public function pubData() {
		if ($this->checkUserRight () == false) {
			return $this->ajaxReturnError ( "没有操作权限" );
		}
		
		// 获取本模块信息
		$moduleObj = $this->getModuleInfo ();
		if ($moduleObj === false) {
			return $this->ajaxReturnError ( "模块不存在或已经删除" );
		}
		
		// 更新数据库
		$optUser = $this->getUserFromSession ( "userName" );
		$dao = new \Search\Model\DirtyWordsModel ();
		if ($dao->updatePubData ( $optUser ) === false) {
			return $this->ajaxReturnError ( "更新发布数据失败" );
		}
		
		// 查询有效数据
		$datalist = $dao->getPubData ();
		
		// 写入文件
		$myfile = fopen ( $this->tempDataFile, "w" );
		if ($myfile === false) {
			return $this->ajaxReturnError ( "打开数据文件失败" );
		}
		
		$myfilesb = fopen ( $this->tempDataFileVsb, "w" );
		if ($myfilesb === false) {
			return $this->ajaxReturnError ( "打开数据文件失败" );
		}
		
		$myfilev2 = fopen ( $this->tempDataFileV2, "w" );
		if ($myfilev2 === false) {
			return $this->ajaxReturnError ( "打开数据文件失败" );
		}
		
		$myfilev3 = fopen ( $this->tempDataFileV3, "w" );
		if ($myfilev3 === false) {
			return $this->ajaxReturnError ( "打开数据文件失败" );
		}
		
		for($i = 0; $i < count ( $datalist ); $i ++) {
			$keyWord = UTF82GBK ( $datalist [$i] ['keyWord'] );
			$timestamp = strtotime ( $datalist [$i] ['avaiTime'] );
			if (fwrite ( $myfilesb, sprintf ( "%s\n", $keyWord ) ) === false) {
				fclose ( $myfile );
				fclose ( $myfilesb );
				fclose ( $myfilev2 );
				fclose ( $myfilev3 );
				return $this->ajaxReturnError ( "写数据文件失败" );
			}
			if ($datalist [$i] ["useType"] == 0) {
				if (fwrite ( $myfile, sprintf ( "%s\n", $keyWord ) ) === false) {
					fclose ( $myfile );
					fclose ( $myfilesb );
					fclose ( $myfilev2 );
					fclose ( $myfilev3 );
					return $this->ajaxReturnError ( "写数据文件失败" );
				}
				if (fwrite ( $myfilev2, sprintf ( "%s\t%d\t%d\n", $keyWord, $datalist [$i] ['expType'], $timestamp ) ) === false) {
					fclose ( $myfile );
					fclose ( $myfilesb );
					fclose ( $myfilev2 );
					fclose ( $myfilev3 );
					return $this->ajaxReturnError ( "写数据文件失败" );
				}
			} else {
				if (fwrite ( $myfilev3, sprintf ( "%s\t%d\t%d\t%d\t%s\n", $keyWord, $datalist [$i] ['expType'], $datalist [$i] ['useType'], $timestamp, $datalist [$i] ['classids'] ) ) === false) {
					fclose ( $myfile );
					fclose ( $myfilesb );
					fclose ( $myfilev2 );
					fclose ( $myfilev3 );
					return $this->ajaxReturnError ( "写数据文件失败" );
				}
			}
		}
		fclose ( $myfile );
		fclose ( $myfilesb );
		fclose ( $myfilev2 );
		fclose ( $myfilev3 );
		// 同步文件
		$output = null;
		$errno = 0;
		$command = sprintf ( "cp %s %s;cp %s %s;cp %s %s;cp %s %s;cd %s;./SynctoOperationSvr.sh sync %s", 
				$this->tempDataFile, $this->pubDataFile, 
				$this->tempDataFileVsb, $this->pubDataFileVsb,
				$this->tempDataFileV2, $this->pubDataFileV2, 
				$this->tempDataFileV3, $this->pubDataFileV3, 
				C ( "PUB_SHELL_PATH" ), $moduleObj ["syncFilePath"] );
		$errinfo = exec ( $command, $output, $errno );
		if ($errno != 0) {
			return $this->ajaxReturnError ( "文件同步失败" );
		}
		$this->writeLog ( $this::LOGTYPE_PUB, "", "" );
		return $this->ajaxReturnSuccess ();
	}
	
	private function checkParam($keyWord, $avaiTime, $expType, $useType, $classids, $pkid) {
		if (! isValidString ( $keyWord )) {
			return array (
					"result" => false,
					"errmsg" => "关键字不能为空" 
			);
		}
		if (! isDateTimeString ( $avaiTime )) {
			return array (
					"result" => false,
					"errmsg" => "有效期格式错误" 
			);
		}
		if ($expType != "0" && $expType != "1") {
			return array (
					"result" => false,
					"errmsg" => "匹配模式无效" 
			);
		}
		if ($useType != "0" && $useType != "1" && $useType != "2") {
			return array (
					"result" => false,
					"errmsg" => "应用类型无效" 
			);
		}
		if (! is_numeric ( $pkid )) {
			return array (
					"result" => false,
					"errmsg" => "主键ID无效" 
			);
		}
		if ($useType == "1" || $useType == "2") {
			$ret = $this->checkClassids ( $classids );
			if ($ret ["result"] != true) {
				return $ret;
			}
		}
		
		$keyWord = mysql_escape_string ( $keyWord );
		$dao = new \Search\Model\DirtyWordsModel ();
		
		if ($expType == "0") { // 全词匹配
			$sql = sprintf ( "select * from sopr_DirtyWords where isDelete=0 and pkId<>%d and keyWord='%s' 
                                        order by expType asc,createTime desc limit 0,10", $pkid, $keyWord );
			$datalist = $dao->query ( $sql );
			if (count ( $datalist ) > 0) {
				return array (
						"result" => false,
						"errmsg" => sprintf ( "关键字“%s”配置冲突，原因如下：", $keyWord ) . $this->getConflictInfo ( $datalist ) 
				);
			}
		}
		if ($expType == "1") { // 部分匹配
			$sql = sprintf ( "select * from sopr_DirtyWords where isDelete=0 and pkId<>%d and 
                                        ((keyWord='%s') or (expType=1 and '%s' like concat('%%',keyWord,'%%')) or (expType=1 and keyWord like '%%%s%%')) 
                                        order by expType asc,createTime desc limit 0,10", $pkid, $keyWord, $keyWord, $keyWord );
			$datalist = $dao->query ( $sql );
			if (count ( $datalist ) > 0) {
				return array (
						"result" => false,
						"errmsg" => sprintf ( "关键字“%s”配置冲突，原因如下：", $keyWord ) . $this->getConflictInfo ( $datalist ) 
				);
			}
		}
		return array (
				"result" => true,
				"errmsg" => "success" 
		);
	}
	
	private function getConflictInfo($datalist) {
		$str = "";
		for($i = 0; $i < count ( $datalist ); $i ++) {
			$str = $str . sprintf ( "\n%d. “%s”(%s+%s)；", $i + 1, $datalist [$i] ["keyWord"], $datalist [$i] ["expType"] == 0 ? "全词匹配" : "部分匹配", 
					$datalist [$i] ["useType"] == 0 ? "全部屏蔽" : ($datalist [$i] ["useType"] == 1 ? "召回指定品类" : "屏蔽指定品类") );
		}
		return $str;
	}
	
	private function checkClassids($classids) {
		if (! isValidString ( $classids )) {
			return array (
					"result" => false,
					"errmsg" => "品类ID不能为空" 
			);
		}
		$datatemp = explode ( ",", $classids );
		$datalist = array ();
		for($i = 0; $i < count ( $datatemp ); $i ++) {
			if (! in_array ( $datatemp [$i], $datalist )) {
				array_push ( $datalist, $datatemp [$i] );
			}
		}
		$datalistlen = count ( $datalist );
		if ($datalistlen < 1) {
			return array (
					"result" => false,
					"errmsg" => "品类ID不能为空" 
			);
		}
		if ($datalistlen > 16) {
			return array (
					"result" => false,
					"errmsg" => "目前最多支持配置16个品类ID" 
			);
		}
		for($i = 0; $i < $datalistlen; $i ++) {
			$classid = $datalist [$i];
			if (! isPositiveNumeric ( $classid )) {
				return array (
						"result" => false,
						"errmsg" => sprintf ( "品类ID[%s]无效", $classid ) 
				);
			}
			if($this->NcaClassInfo($classid)===false){
				return array (
						"result" => false,
						"errmsg" => sprintf ( "品类ID[%s]无效", $classid )
				);
			}
		}
		return array (
				"result" => true,
				"errmsg" => "success" 
		);
	}
	
	private  function NcaClassInfo($classId){
		if(isPositiveNumeric($classId)){
			$dao=new \Search\Model\ClassInfoModel();
			$data=$dao->getClassInfo($classId);
			if(count($data)==0){
				return false;
			}
			return $data;
		}
		else{
			return false;
		}
	}
	
	private function deleteRepeatClassid($classids) {
		$classids = str_replace ( "，", ",", $classids );
		$datatemp = explode ( ",", $classids );
		$datalist = array ();
		$str = "";
		for($i = 0; $i < count ( $datatemp ); $i ++) {
			if (isValidString ( $datatemp [$i] ) && ! in_array ( $datatemp [$i], $datalist )) {
				array_push ( $datalist, $datatemp [$i] );
				$str = $str . $datatemp [$i] . ",";
			}
		}
		$strnum = strlen ( $str );
		if ($strnum >= 1 && $str [$strnum - 1] == ",") {
			return substr ( $str, 0, $strnum - 1 );
		}
		return $str;
	}
}
