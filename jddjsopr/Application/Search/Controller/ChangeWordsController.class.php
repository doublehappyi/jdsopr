<?php
namespace Search\Controller;
use Think\Controller;
use Common\Controller\SoprController;
use Org\Net\Http;

class ChangeWordsController extends SoprController {
	
	 private $tempfile;
	 private $pubfile;
	 private $uploadConfig;
	
	function __construct(){
		parent::__construct();
		$this->moduleKey="changewords";
		$this->tempfile=C('WEB_ROOT_PATH')."/sysfiles/publish/changewords.txt";
		$this->pubfile=C('PUB_DATA_PATH')."/changewords/changewords.txt";
		$this->uploadConfig= array('maxSize' => 524288,'rootPath' => './sysfiles/','subName' => 'upload','exts' => array('txt'));
	}
	
	public function index(){
		$this->display("index");
	}
	
	/**
	 * 查询纠错词
	 */
    public function getChangeWords(){
    	$keyword = trim ( I ( "keyword" ) );
    	$page = trim ( I ( "page" ) );
    	$pagesize = trim ( I ( "pagesize" ) );
    	
    	$page = isPositiveNumeric ( $page ) ? intval ( $page ) : 0;
    	$pagesize = isPositiveNumeric ( $pagesize ) ? intval ( $pagesize ) : $this::PAGESIZE_DEFAULT;
    	
    	$dao=new \Search\Model\ChangeWordsModel();
    	$data=$dao->getChangeWords($keyword, $page, $pagesize);
    	$page=$dao->getChangeWordsPageInfo($keyword, $page, $pagesize);
    	return $this->ajaxReturnSuccess ( $data, $page);
    }
    
    /**
     * 查询当前用户保存未发布纠错词
     */
    public function getUnPubChangeWords(){
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\ChangeWordsModel();
    	$data=$dao->getUnPubData($optUser);
    	return $this->ajaxReturnSuccess ( $data );
    }
    
    private function SaveOneChangeWords($keyword,$avaidatetime,$optUser,$content){
    	if(!isValidString($keyword)){
    		return "关键字无效";
    	}
    	if(!isDateTimeString($avaidatetime)){
    		return "有效期无效";
    	}
    	if(!isValidString($content)){
    		return "纠错词不能为空";
    	}
    	$dao = new \Search\Model\ChangeWordsModel();
    	$ret=$dao->saveChangeWords($keyword, $avaidatetime, $content, $optUser);
    	if($ret===false){
    		return "保存失败".$dao->getDbError();
    	}
    	$this->writeLog($this::LOGTYPE_MODIFY,$keyword,sprintf("avaiDateTime:%s,keyValue:%s",$avaidatetime,$content));
    	return true;
    }
    
    /**
     * 保存纠错词
     */
    public  function saveChangeWords(){
      if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$keyword = trim ( I ( "keyword" ) );
    	$avaidatetime= trim ( I ( "avaidatetime" ) );
    	$content=trim ( I ( "changeword" ) );
    	$optUser=$this->getUserFromSession("userName");
    	if(!isValidString($avaidatetime)){
    		$avaidatetime=$this::AVAILABLE_DATETIME_MAXED;
    	}
    	$ret=$this->SaveOneChangeWords($keyword, $avaidatetime, $optUser, $content);
    	if($ret===true){
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError($ret);
    }
    
    /**
     * 删除纠错词
     */
    public function delChangeWords(){
      if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$keyword = trim ( I ( "keyId" ) );
    	if(!isPositiveNumeric($keyword)){
    		return $this->ajaxReturnError("输入参数无效");
    	}
    	
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\ChangeWordsModel();
    	$ret=$dao->delChangeWords($keyword, $optUser);
    	if($ret===true){
    		$this->writeLog($this::LOGTYPE_DELETE, "",sprintf("keyId:%d",$keyword));
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError($dao->getError());
    }
    
    /**
     * 发布纠错词
     */
    public  function pubChangeWords(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	//获取本模块信息
    	$moduleObj=$this->getModuleInfo();
    	if($moduleObj===false){
    		return $this->ajaxReturnError ( "模块不存在或已经删除");
    	}
    	
    	//更新发布状态
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\ChangeWordsModel();
    	if($dao->updatePubData($optUser)===false){
    		return $this->ajaxReturnError ( "更新发布状态失败");
    	}
    	
    	//写文件
    	$datalist=$dao->getPubData();
    	$myfile=fopen($this->tempfile,"w");
    	if($myfile===false){
    		return $this->ajaxReturnError ( "打开写文件失败");
    	}
    	$format="%s\t%s\n";
    	for($i=0;$i<count($datalist);$i++){
    		$keyword=UTF82GBK($datalist[$i]["keyName"]);
    		$synonym=str_replace(":", "\t", UTF82GBK($datalist[$i]["keyValue"]));
    		$content=sprintf($format,$keyword,$synonym);
    		if(fwrite($myfile, $content)===false){
    			fclose($myfile);
    			return $this->ajaxReturnError ( "写发布文件失败");
    		}
    	}
    	fclose($myfile);
    	
    	//推送脚本
    	$output=null;
    	$errno=0;
    	$command=sprintf("cp -f %s %s;cd %s;./SynctoOperationSvr.sh sync %s",$this->tempfile,$this->pubfile,C("PUB_SHELL_PATH"),$moduleObj["syncFilePath"]);
    	$errinfo=exec($command,$output,$errno);
    	if($errno!=0){
    		return $this->ajaxReturnError ( "文件同步失败");
    	}
    	$this->writeLog($this::LOGTYPE_PUB, "", "");
    	return $this->ajaxReturnSuccess ();
    }
    
    /**
     * 导出纠错词
     */
    public function exportChangeWords(){
    	Http::download($this->tempfile);
    }
    
    /**
     * 导入纠错词
     */
    public function importChangeWords(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	$upload = new \Think\Upload($this->uploadConfig);// 实例化上传类
    	$info = $upload->uploadOne($_FILES['uploadHotClass']);//上传文件
    	if($info===false){
    		return $this->ajaxReturnError ($upload->getError());
    	}
    	$fileName=$upload->rootPath.$info['savepath'].$info['savename'];
    	if(!($filehandle = fopen($fileName, "r"))){
    		return $this->ajaxReturnError ("文件读取失败");
    	}
    	$validline=0;
    	$rowindex=0;
    	$optUser=$this->getUserFromSession("userName");
    	while(!feof($filehandle))
    	{
    		$rowindex++;
    		$strline=fgets($filehandle);
    		$datalist=explode("\t",$strline);
    		$keyword="";
    		$content="";
    		$avaiTime=$this::AVAILABLE_DATETIME_MAXED;
    		if(count($datalist)==2 || count($datalist)==3){
	    		if(count($datalist)==2)
	    		{
	    			$keyword=iconv("GBK", "UTF-8//IGNORE", trim($datalist[0]));
	    			$content=iconv("GBK", "UTF-8//IGNORE", trim($datalist[1]));
	    		}
	    		if(count($datalist)==3)
	    		{
	    			$keyword=iconv("GBK", "UTF-8//IGNORE", trim($datalist[0]));
	    			$avaiTime=trim($datalist[1]);
	    			$content=iconv("GBK", "UTF-8//IGNORE", trim($datalist[2]));
	    		}
    			$ret=$this->SaveOneChangeWords($keyword, $avaiTime, $optUser, $content);
    			if($ret===true){
    				$validline++;
    			}else{
    				fclose($filehandle);
    				unlink($fileName);
    				return $this->ajaxReturnError(sprintf("Row%d,导入失败:%s",$rowindex,$ret));
    			}
    		}else{
    			fclose($filehandle);
    			unlink($fileName);
    			return $this->ajaxReturnError(sprintf("Row%d,导入失败:字段格式错误",$rowindex));
    		}
    	}
    	fclose($filehandle);
    	unlink($fileName);
    	return $this->ajaxReturnSuccess($validline);
    }
}