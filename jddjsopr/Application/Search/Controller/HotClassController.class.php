<?php
namespace Search\Controller;
use Think\Controller;
use Common\Controller\SoprController;
use Org\Net\Http;

class HotClassController extends SoprController {
	
	 private $tempfile;
	 private $pubfile;
	 private $uploadConfig;
	
	function __construct(){
		parent::__construct();
		$this->moduleKey="hotclass";
		$this->tempfile=C('WEB_ROOT_PATH')."/sysfiles/publish/hot_class_user.txt";
		$this->pubfile=C('PUB_DATA_PATH')."/hotclass/hot_class_user.txt";
		$this->uploadConfig= array('maxSize' => 524288,'rootPath' => './sysfiles/','subName' => 'upload','exts' => array('txt'));
	}
	
	public function index(){
		$this->display("index");
	}
	
	private  function NcaClassInfo($classId){
		if(isPositiveNumeric($classId)){
			$dao=new \Search\Model\ClassInfoModel();
			$data=$dao->getClassInfo($classId);
			if(count($data)==0){
				return false;
			}
			return $data;
		}
		else{
			return false;
		}
	}
	
	/**
	 * 类目ID查询类目信息
	 */
	public function getClassInfo(){
		$classid=trim ( I ( "classId" ) );
		$ret=$this->NcaClassInfo($classid);
		if($ret==false){
			return $this->ajaxReturnError(sprintf("类目ID:%s无效",$classid));
		}else{
			return $this->ajaxReturnSuccess($ret);
		}
	}
	
	/**
	 * 查询自动热门类目
	 */
    public function getSysHotClass(){
    	$keyword = trim ( I ( "keyword" ) );
    	if(isValidString($keyword)){
    		$dao = new \Search\Model\HotClassSysModel();
    		$data=$dao->getHotClass($keyword,false);
    		return $this->ajaxReturnSuccess ( $data );
    	}
    	return $this->ajaxReturnError("请求的关键字无效");
    }
    
    /**
     * 查询人工热门类目
     */
    public function getUserHotClass(){
    	$keyword = trim ( I ( "keyword" ) );
    	if(isValidString($keyword)){
    		$dao = new \Search\Model\HotClassUserModel();
    		$data=$dao->getHotClass($keyword,false);
    		return $this->ajaxReturnSuccess ( $data );
    	}
    	return $this->ajaxReturnError("请求的关键字无效");
    }
    
    /**
     * 查询当前用户保存未发布热门类目
     */
    public function getUnPubHotClass(){
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\HotClassUserModel();
    	$data=$dao->getUnPubHotClass($optUser);
    	return $this->ajaxReturnSuccess ( $data );
    }
    
    private function SaveOneHotClass($keyword,$avaidatetime,$optUser,$content){
    	if(!isValidString($keyword)){
    		return "关键字无效";
    	}
    	if(!isDateTimeString($avaidatetime)){
    		return "有效期无效";
    	}
    	$totalScore=0;
    	$mapClassInfo=array();
    	$tmparray1=explode('-',$content);//classid1:score1-classid2:score2
    	for($i=0;$i<count($tmparray1);$i++){
    		$tmpstr=$tmparray1[$i];
    		$tmparray2=explode(':',$tmpstr);
    		if(count($tmparray2)==2 && $this->NcaClassInfo($tmparray2[0])!=false && is_numeric($tmparray2[1])){
    			$mapClassInfo[$tmparray2[0]]=$tmparray2[1];
    			$totalScore+=intval($tmparray2[1]);
    		}else{
    			return "classId:classScore格式错误";
    		}
    	}
    	if($totalScore>10000){
    		return "类目总分须小于10000";
    	}
    	 
    	$dao = new \Search\Model\HotClassUserModel();
    	$ret=$dao->saveHotClass($keyword, $avaidatetime, $optUser, $mapClassInfo);
    	if($ret===false){
    		return "保存失败".$dao->getDbError();
    	}
    	$this->writeLog($this::LOGTYPE_MODIFY,$keyword,sprintf("avaiDateTime:%s,%s",$avaidatetime.$content));
    	return true;
    }
    
    /**
     * 保存人工热门类目
     */
    public  function saveUserHotClass(){
      if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$keyword = trim ( I ( "keyword" ) );
    	$avaidatetime= trim ( I ( "avaidatetime" ) );
    	$content=trim ( I ( "content" ) );
    	$optUser=$this->getUserFromSession("userName");
    	$ret=$this->SaveOneHotClass($keyword, $avaidatetime, $optUser, $content);
    	if($ret===true){
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError($ret);
    }
    
    /**
     * 发布热门类目
     */
    public  function pubUserHotClass(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	//获取本模块信息
    	$moduleObj=$this->getModuleInfo();
    	if($moduleObj===false){
    		return $this->ajaxReturnError ( "模块不存在或已经删除");
    	}
    	
    	//更新发布状态
    	$optUser=$this->getUserFromSession("userName");
    	$dao = new \Search\Model\HotClassUserModel();
    	if($dao->PubHotClass($optUser)===false){
    		return $this->ajaxReturnError ( "更新发布状态失败");
    	}
    	
    	//写文件
    	$datalist=$dao->getPubHotClass();
    	$myfile=fopen($this->tempfile,"w");
    	if($myfile===false){
    		return $this->ajaxReturnError ( "打开写文件失败");
    	}
    	$format="%s\t%d\t%d\t%d\n";
    	for($i=0;$i<count($datalist);$i++){
    		$keyword=UTF82GBK($datalist[$i]["keyName"]);
    		$timestamp=strtotime($datalist[$i]["avaiDateTime"]);
    		$content=sprintf($format,$keyword,$timestamp,$datalist[$i]["classId"],$datalist[$i]["classPoint"]);
    		if(fwrite($myfile, $content)===false){
    			fclose($myfile);
    			return $this->ajaxReturnError ( "写发布文件失败");
    		}
    	}
    	fclose($myfile);
    	
    	//推送脚本
    	$output=null;
    	$errno=0;
    	$command=sprintf("cp -f %s %s;cd %s;./SynctoOperationSvr.sh sync %s",$this->tempfile,$this->pubfile,C("PUB_SHELL_PATH"),$moduleObj["syncFilePath"]);
    	$errinfo=exec($command,$output,$errno);
    	if($errno!=0){
    		return $this->ajaxReturnError ( "文件同步失败");
    	}
    	$this->writeLog($this::LOGTYPE_PUB, "", "");
    	return $this->ajaxReturnSuccess ();
    }
    
    /**
     * 导出热门类目
     */
    public function exportHotClass(){
    	Http::download($this->tempfile);
    }
    
    /**
     * 导入热门类目
     */
    public function importHotClass(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	$upload = new \Think\Upload($this->uploadConfig);// 实例化上传类
    	$info = $upload->uploadOne($_FILES['uploadHotClass']);//上传文件
    	if($info===false){
    		return $this->ajaxReturnError ($upload->getError());
    	}
    	$fileName=$upload->rootPath.$info['savepath'].$info['savename'];
    	if(!($filehandle = fopen($fileName, "r"))){
    		return $this->ajaxReturnError ("文件读取失败");
    	}
    	$validline=0;
    	$rowindex=0;
    	$optUser=$this->getUserFromSession("userName");
    	while(!feof($filehandle))
    	{
    		$rowindex++;
    		$strline=fgets($filehandle);
    		$datalist=explode("\t",$strline);
    		if(count($datalist)==3)
    		{
    			$keyword=iconv("GBK", "UTF-8//IGNORE", trim($datalist[0]));
    			$avaiTime=trim($datalist[1]);
    			$content=trim($datalist[2]);
    			$ret=$this->SaveOneHotClass($keyword, $avaiTime, $optUser, $content);
    			if($ret===true){
    				$validline++;
    			}else{
    				fclose($filehandle);
    				unlink($fileName);
    				return $this->ajaxReturnError(sprintf("Row%d,导入失败:%s",$rowindex,$ret));
    			}
    		}else{
    			fclose($filehandle);
    			unlink($fileName);
    			return $this->ajaxReturnError(sprintf("Row%d,导入失败:字段格式错误",$rowindex));
    		}
    	}
    	fclose($filehandle);
    	unlink($fileName);
    	return $this->ajaxReturnSuccess($validline);
    }
}