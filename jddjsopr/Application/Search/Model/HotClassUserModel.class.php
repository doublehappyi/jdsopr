<?php

namespace Search\Model;

use Common\Model\SoprModel;

class HotClassUserModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_HotClassUser";
	
	public function getHotClass($keyword,$islike=true) {
		$sql = "select a.*,ifNUll(CONCAT(b.className1,'/',b.className2,'/',b.className3),'unkonw') as className from sopr_HotClassUser a 
				left join sopr_ClassInfo b on a.classId=b.classId3 
				where a.isDelete=0 ";
		if ($keyword != "") {
			if($islike==true){
				$sql = $sql . sprintf ( " and a.keyName like '%%%s%%' ", mysql_escape_string ( $keyword ) );
			}else{
				$sql = $sql . sprintf ( " and a.keyName='%s' ", mysql_escape_string ( $keyword ) );
			}
		}
		$sql = $sql . "order by CAST(a.classPoint AS SIGNED) desc limit 0,30 ";
		return $this->query ( $sql );
	}
	
	public function getUnPubHotClass($optUser){
		$sqlformat = "select a.*,ifNUll(CONCAT(b.className1,'/',b.className2,'/',b.className3),'unkonw') as className from sopr_HotClassUser a 
				left join sopr_ClassInfo b on a.classId=b.classId3 
				where a.isDelete=0 and a.isPub=0 and a.optUser='%s' 
				order by a.keyName,CAST(a.classPoint AS SIGNED) desc";
		return $this->query ( sprintf($sqlformat,$optUser));
	}
	
	public function getPubHotClass(){
		$sql = "select * from sopr_HotClassUser where isDelete=0 and isPub=1 and avaiDateTime>now() order by keyName,CAST(classPoint AS SIGNED) desc";
		return $this->query ( $sql);
	}
	
	private function delHotClass($keyword,$optUser){
		$sqlformat="update sopr_HotClassUser set isDelete=1,optUser='%s' where keyName='%s'";
		$sql=sprintf($sqlformat,$optUser,mysql_escape_string ( $keyword ));
		return $this->execute($sql);
	}
	
	private function addHotClass($keyword,$classid,$classPoint,$avaidatetime,$optUser){
		$sqlformat="insert into sopr_HotClassUser(keyName,classId,classPoint,avaiDateTime,optUser) values('%s','%s','%s','%s','%s')";
		$sql=sprintf($sqlformat,mysql_escape_string ( $keyword ),$classid,$classPoint,$avaidatetime,$optUser);
		return $this->execute($sql);
	}
	
	public function saveHotClass($keyword,$avaidatetime,$optUser,$mapClassInfo){
		$keyword=mysql_escape_string ( $keyword );
		try{
			$this->startTrans();
			if($this->delHotClass($keyword, $optUser)===false){
				$this->rollback();
				return  false;
			}
			if(is_array($mapClassInfo) && count($mapClassInfo)>0){
				foreach ($mapClassInfo as $key=>$value){
					if($this->addHotClass($keyword, $key, $value, $avaidatetime, $optUser)===false){
						$this->rollback();
						return  false;
					}
				}
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function PubHotClass($optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_HotClassUser set isPub=1 where optUser='%s' and isPub=0 and isDelete=0";
			if($this->execute(sprintf($sqlformat,$optUser))===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
}
