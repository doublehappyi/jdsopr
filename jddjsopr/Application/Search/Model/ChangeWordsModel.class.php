<?php

namespace Search\Model;

use Think\Model;

class ChangeWordsModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_ChangeWords";
	
	private function makeSelectSQL($keyWord){
		$keyWord=mysql_escape_string($keyWord);
		$sql="select * from sopr_ChangeWords where isDelete=0 ";
		if($keyWord!=""){
			$sql=$sql.sprintf(" and (keyName='%s' or keyValue like '%%%s%%') ",$keyWord,$keyWord);
		}
		return $sql;
	}
	
	public function getChangeWordsPageInfo($keyWord, $page,$pagesize) {
		$sql = sprintf("select count(*) as num from (%s) t",$this->makeSelectSQL($keyWord));
		$list = $this->query ( $sql );
		$num=intval($list[0]["num"]);
		$pageinfo['TotalNum']=$num;
		$pageinfo['CurrentPage']=$page;
		$pageinfo['PageSize']=$pagesize;
		$temp=intval($num/$pagesize);
		if(0!=($num%$pagesize))
		{
			$temp+=1;
		}
		$pageinfo['TotalPage']=$temp;
		
		return $pageinfo;
	}
	
	public function getChangeWords($keyWord,$page,$pagesize){
		$sql=$this->makeSelectSQL($keyWord);
		$sql=$sql.sprintf(" order by createTime desc limit %d,%d ", $page*$pagesize, $pagesize);
		return $this->query($sql);
	}
	
	public function saveChangeWords($keyWord,$avaiTime,$synonym ,$optUser){
		try{
			$keyWord=mysql_escape_string($keyWord);
			$synonym=mysql_escape_string($synonym);
			$sql=sprintf("select keyId from sopr_ChangeWords where keyName='%s'",$keyWord);
			$olddata=$this->query($sql);
			$this->startTrans();
			if(count($olddata)==0){
				$sqlformat="insert into sopr_ChangeWords(keyName,avaiDateTime,keyValue,optUser,createTime)
					 values ('%s','%s','%s','%s','%s') ";
				$sql=sprintf($sqlformat,$keyWord,$avaiTime,$synonym,$optUser,date('Y-m-d H:i:s'));
			}else{
				$sqlformat="update sopr_ChangeWords set keyName='%s',avaiDateTime='%s',isPub=0,isDelete=0,
						optUser='%s',keyValue='%s',createTime='%s' where keyId=%d ";
				$sql=sprintf($sqlformat,$keyWord,$avaiTime,$optUser,$synonym,date('Y-m-d H:i:s'),$olddata[0]["keyId"]);
			}
			
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function delChangeWords($pkId, $optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_ChangeWords set isDelete=1,optUser='%s' where keyId=%d";
			$sql=sprintf($sqlformat,mysql_escape_string($optUser), $pkId);
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function updatePubData($optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_ChangeWords set isPub=1 where isDelete=0 and optUser='%s'";
			$sql=sprintf($sqlformat,mysql_escape_string($optUser));
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function getPubData(){
		$sql="select * from sopr_ChangeWords where isDelete=0 and isPub=1 and avaiDateTime>now() order by createTime desc";
		return $this->query($sql);
	}
	
	public function getUnPubData($optUser){
		$sql="select * from sopr_ChangeWords where isDelete=0 and isPub=0 and optUser='%s' order by createTime desc";
		return $this->query(sprintf($sql,$optUser));
	}
}
