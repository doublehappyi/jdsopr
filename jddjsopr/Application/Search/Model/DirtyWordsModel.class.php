<?php

namespace Search\Model;

use Think\Model;

class DirtyWordsModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_DirtyWords";
	
	private function makeSelectSQL($pkId,$keyWord,$expType,$useType,$islike){
		$keyWord=mysql_escape_string($keyWord);
		$sql="select * from sopr_DirtyWords where isDelete=0 ";
		if($pkId>=0){
			$sql=$sql.sprintf(" and pkId=%d ",$pkId);
		}
		if($keyWord!=""){
			if($islike==true)
				$sql=$sql.sprintf(" and keyWord like '%%%s%%' ",$keyWord);
			else
				$sql=$sql.sprintf(" and keyWord='%s' ",$keyWord);
		}
		if($expType>=0){
			$sql=$sql.sprintf(" and expType=%d ",$expType);
		}
		if($useType>=0){
			$sql=$sql.sprintf(" and useType=%d ",$useType);
		}
		return $sql;
	}
	
	public function getDirtyWordsPageInfo($pkId, $keyWord, $expType, $useType, $islike, $page,$pagesize) {
		$sql = sprintf("select count(*) as num from (%s) t",$this->makeSelectSQL($pkId,$keyWord,$expType,$useType,$islike));
		$list = $this->query ( $sql );
		$num=intval($list[0]["num"]);
		$pageinfo['TotalNum']=$num;
		$pageinfo['CurrentPage']=$page;
		$pageinfo['PageSize']=$pagesize;
		$temp=intval($num/$pagesize);
		if(0!=($num%$pagesize))
		{
			$temp+=1;
		}
		$pageinfo['TotalPage']=$temp;
		
		return $pageinfo;
	}
	
	public function getDirtyWords($pkId,$keyWord,$expType,$useType,$islike,$page,$pagesize){
		$sql=$this->makeSelectSQL($pkId,$keyWord,$expType,$useType,$islike);
		$sql=$sql.sprintf(" order by createTime desc limit %d,%d ", $page*$pagesize, $pagesize);
		return $this->query($sql);
	}
	
	public function saveDirtyWords($pkId,$keyWord,$avaiTime,$expType, $useType,$classids,$optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_DirtyWords set keyWord='%s',avaiTime='%s',expType=%d,useType=%d,classids='%s',
				isPub=0,isDelete=0,optUser='%s',createTime='%s' where pkId=%d ";
			$sql=sprintf($sqlformat,mysql_escape_string($keyWord),mysql_escape_string($avaiTime),$expType,$useType,
				mysql_escape_string($classids), mysql_escape_string($optUser),date('Y-m-d H:i:s'),$pkId);
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function addDirtyWords($keyWord,$avaiTime,$expType, $useType,$classids,$optUser){
		$sql=sprintf("select * from sopr_DirtyWords where keyWord='%s' and isDelete=0",mysql_escape_string($keyWord));
		$list=$this->query($sql);
		if(count($list)>0){
			$this->error="关键字已经存在，请勿重复保存";
			return false;
			//return $this->saveDirtyWords($list[0]["pkId"],$keyWord,$avaiTime,$expType,$useType,$classids,$optUser);
		}else{
			try{
				$this->startTrans();
				$sqlformat="insert into sopr_dirtykeyword(keyWord,createTime,avaiTime,expType,useType,classids,optUser)
					 values ('%s','%s','%s',%d,%d,'%s','%s') ";
				$sql=sprintf($sqlformat,mysql_escape_string($keyWord),date('Y-m-d H:i:s'),mysql_escape_string($avaiTime),
					$expType, $useType,mysql_escape_string($classids),mysql_escape_string($optUser));
				if($this->execute($sql)===false){
					$this->rollback();
					return  false;
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
	}
	
	public function delDirtyWords($pkId, $optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_DirtyWords set isDelete=1,optUser='%s' where pkId=%d";
			$sql=sprintf($sqlformat,mysql_escape_string($optUser), $pkId);
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function updatePubData($optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_DirtyWords set isPub=1 where isDelete=0 and optUser='%s'";
			$sql=sprintf($sqlformat,mysql_escape_string($optUser));
			if($this->execute($sql)===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function getPubData(){
		$sql="select * from sopr_DirtyWords where isDelete=0 and isPub=1 and avaiTime>now() order by createTime desc";
		return $this->query($sql);
	}
}
