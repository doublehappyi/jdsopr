<?php

namespace Search\Model;

use Common\Model\SoprModel;

class HotClassSysModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_HotClassSys";
	
	public function getHotClass($keyword,$islike=true) {
		$sql = "select a.*,ifNUll(CONCAT(b.className1,'/',b.className2,'/',b.className3),'unkonw') as className from sopr_HotClassSys a 
				left join sopr_ClassInfo b on a.classId=b.classId3 where 1=1 ";
		if ($keyword != "") {
			if($islike==true){
				$sql = $sql . sprintf ( " and a.keyName like '%%%s%%' ", mysql_escape_string ( $keyword ) );
			}else{
				$sql = $sql . sprintf ( " and a.keyName='%s' ", mysql_escape_string ( $keyword ) );
			}
		}
		$sql = $sql . "order by CAST(a.classPoint AS SIGNED) desc limit 0,30 ";
		return $this->query ( $sql );
	}
}
