<?php

namespace Search\Model;

use Common\Model\SoprModel;

class HotWordsModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_HotWords";
	const INTERVENTION_TYPE_AUTO=0;//系统
	const INTERVENTION_TYPE_USER=1;//人工
	const INTERVENTION_TYPE_FILTER=2;//屏蔽
	
	private function makeSelectSQL($intervention,$keywords,$org_code,$store_id){
		$keywords=mysql_escape_string($keywords);
		$sql="select * from sopr_HotWords where isDelete=0 ";
		if(isPositiveNumeric($intervention)){
			$sql=$sql.sprintf(" and intervention=%d ",$intervention);
		}
		if(isPositiveNumeric($org_code)){
			$sql=$sql.sprintf(" and org_code=%d ",$org_code);
		}
		if(isPositiveNumeric($store_id)){
			$sql=$sql.sprintf(" and store_id=%d ",$store_id);
		}
		if(isValidString($keywords)){
			$sql=$sql.sprintf(" and keywords='%s' ",$keywords);
		}
		return $sql;
	}
	
	public function getHotWordsPageInfo($intervention,$keywords,$org_code,$store_id, $page,$pagesize) {
		$sql = sprintf("select count(*) as num from (%s) t",$this->makeSelectSQL($intervention,$keywords,$org_code,$store_id));
		$list = $this->query ( $sql );
		$num=intval($list[0]["num"]);
		$pageinfo['TotalNum']=$num;
		$pageinfo['CurrentPage']=$page;
		$pageinfo['PageSize']=$pagesize;
		$temp=intval($num/$pagesize);
		if(0!=($num%$pagesize))
		{
			$temp+=1;
		}
		$pageinfo['TotalPage']=$temp;
		return $pageinfo;
	}
	
	public function getHotWords($intervention,$keywords,$org_code,$store_id, $page,$pagesize) {
		$sql=$this->makeSelectSQL($intervention,$keywords,$org_code,$store_id);
		$sql=$sql.sprintf(" order by sort_num asc,create_time desc limit %d,%d ", $page*$pagesize, $pagesize);
		return $this->query($sql);
	}
	
	public function getUnPubHotWords($optUser){
		$sqlformat = "select * from sopr_HotWords a
				where a.isDelete=0 and a.isPub=0 and a.optUser='%s'
				order by a.sort_num asc,create_time desc";
		return $this->query ( sprintf($sqlformat,$optUser));
	}
	
	public function getPubHotWords(){
		$sql = "select * from sopr_HotWords where isDelete=0 and isPub=1 and intervention<>2 and endTime>now()";
		return $this->query ( $sql);
	}
	
	public function delHotWords($ids,$optUser){
		try{
			$this->startTrans();
			if(is_array($ids) && count($ids)>0){
				$sqlformat="update sopr_HotWords set isDelete=1,optUser='%s' where id='%d'";
				for($i=0;$i<count($ids);$i++){
					$sql=sprintf($sqlformat,$optUser,$ids[$i]);
					if($this->execute($sql)===false){
						$this->rollback();
						return  false;
					}
				}
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function filterHotWords($ids,$optUser){
		try{
			$this->startTrans();
			if(is_array($ids) && count($ids)>0){
				$sqlformat="update sopr_HotWords set intervention=%d,optUser='%s' where id='%d'";
				for($i=0;$i<count($ids);$i++){
					$sql=sprintf($sqlformat,$this::INTERVENTION_TYPE_FILTER,$optUser,$ids[$i]);
					if($this->execute($sql)===false){
						$this->rollback();
						return  false;
					}
				}
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}

	/**
	 * 保存为人工热词
	 */
	public function saveUserHotWords($keywords,$store_ids,$starttime,$endtime,$optUser){
		try{
			$keywords=mysql_escape_string($keywords);
			$this->startTrans();
			if(is_array($store_ids) && count($store_ids)>0){
				for($i=0;$i<count($store_ids);$i++){
					$sqlformat="select id from sopr_HotWords where keywords='%s' and store_id='%d'";
					$sql=sprintf($sqlformat,$keywords,$store_ids[$i]);
					$ret=$this->query($sql);
					if(count($ret)==0){
						//新增
						$sqlformat="insert into sopr_HotWords(keywords,intervention,store_id,create_time,startTime,endTime,optUser)
								values('%s',%d,%d,'%s','%s','%s','%s')";
						$sql=sprintf($sqlformat,$keywords,$this::INTERVENTION_TYPE_USER,$store_ids[$i],date('Y-m-d H:i:s'),$starttime,$endtime,$optUser);
						if($this->execute($sql)===false){
							$this->rollback();
							return  false;
						}
					}else{
						//修改
						$sqlformat="update sopr_HotWords set intervention=%d,isDelete=0,isPub=0,create_time='%s',startTime='%s',endTime='%s',optUser='%s' where id=%d";
						$sql=sprintf($sqlformat,$this::INTERVENTION_TYPE_USER,date('Y-m-d H:i:s'),$starttime,$endtime,$optUser,$ret[0][id]);
						if($this->execute($sql)===false){
							$this->rollback();
							return  false;
						}
					}
				}
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}

	/**
	 * 保存为屏蔽热词
	 */
	public function saveFilterHotWords($keywords,$store_ids,$optUser){
		try{
			$keywords=mysql_escape_string($keywords);
			$this->startTrans();
			if(is_array($store_ids) && count($store_ids)>0){
				//屏蔽指定门店
				for($i=0;$i<count($store_ids);$i++){
					$sqlformat="select id from sopr_HotWords where keywords='%s' and store_id='%d'";
					$sql=sprintf($sqlformat,$keywords,$store_ids[$i]);
					$ret=$this->query($sql);
					if(count($ret)==0){
						//新增
						$sqlformat="insert into sopr_HotWords(keywords,intervention,store_id,create_time,startTime,endTime,optUser)
								values('%s',%d,%d,'%s','%s','%s','%s')";
						$sql=sprintf($sqlformat,$keywords,$this::INTERVENTION_TYPE_FILTER,$store_ids[$i],date('Y-m-d H:i:s'),$starttime,$endtime,$optUser);
						if($this->execute($sql)===false){
							$this->rollback();
							return  false;
						}
					}else{
						$sqlformat="update sopr_HotWords set intervention=%d,isPub=0,isDelete=0,create_time='%s',optUser='%s' where id=%d ";
						$sql=sprintf($sqlformat,$this::INTERVENTION_TYPE_FILTER,date('Y-m-d H:i:s'),$optUser,$ret[0]["id"]);
						if($this->execute($sql)===false){
							$this->rollback();
							return  false;
						}
					}
				}
			}else{
				//屏蔽所有门店
				$sqlformat="update sopr_HotWords set intervention=%d,isPub=0,create_time='%s',optUser='%s' where keywords='%s' and isDelete=0 ";
				$sql=sprintf($sqlformat,$this::INTERVENTION_TYPE_FILTER,date('Y-m-d H:i:s'),$optUser,$keywords);
				if($this->execute($sql)===false){
					$this->rollback();
					return  false;
				}
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}

	public function PubHotWords($optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_HotWords set isPub=1 where optUser='%s' and isPub=0 and isDelete=0 and intervention<>2";
			if($this->execute(sprintf($sqlformat,$optUser))===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
	
	public function changeSortNum($id,$sort,$optUser){
		try{
			$this->startTrans();
			$sqlformat="update sopr_HotWords set sort_num=%d,optUser='%s' where id=%d ";
			if($this->execute(sprintf($sqlformat,$sort,$optUser,$id))===false){
				$this->rollback();
				return  false;
			}
			$this->commit();
			return true;
		}catch (\Exception $e){
			$this->rollback();
			return  false;
		}
	}
}
