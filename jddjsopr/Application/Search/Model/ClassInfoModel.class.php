<?php

namespace Search\Model;

use Common\Model\SoprModel;

class ClassInfoModel extends \Common\Model\SoprModel {
	protected  $trueTableName="sopr_ClassInfo";
	
	public function getClassInfo($classId3) {
		$sql = sprintf("select * from sopr_ClassInfo where classId3=%d ",$classId3);
		return $this->query ( $sql );
	}
}
