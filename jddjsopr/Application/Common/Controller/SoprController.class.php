<?php
namespace Common\Controller;

use Think\Controller;
use Think\Model;

class SoprController extends Controller{
	
	const LOGTYPE_LOGIN="登录";
	const LOGTYPE_LOGOUT="退出";
	const LOGTYPE_NEW="新增";
	const LOGTYPE_DELETE="删除";
	const LOGTYPE_MODIFY="修改";
	const LOGTYPE_PUB="发布";
	const LOGTYPE_EXPORT="导出";
	const LOGTYPE_IMPORT="导入";
	const LOGTYPE_REFRESH="刷新";
	
	const PAGESIZE_DEFAULT=15;
	const PAGESIZE_MAXED=10000;
	
	const AVAILABLE_DATETIME_MAXED="2037-12-31 23:59:59";
	
	protected $moduleKey="";
	private  $soprLogDao=null;
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Ajax Json 返回错误信息
	 * @param unknown $errmsg
	 */
	protected  function ajaxReturnError($errmsg) {
		$ret = array ();
		$ret ["result"] = false;
		$ret ["errmsg"] = $errmsg;
		$this->ajaxReturn ( $ret, "JSON" );
		return;
	}
	
	/**
	 * 返回弹出消息页面
	 * @param unknown $errmsg
	 */
	protected function returnPopMsgPage($errmsg){
		if(isNoValidString($errmsg)){
			$errmsg="Sorry,System Occur An Error.";
		}
		echo sprintf("
		<html><head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<title>Error Page</title>
		</head>
		<body onload=\"javascript:alert('%s');javascript:window.close();\"/>
		</html>",$errmsg);
		return;
	}
	
	/**
	 * Ajax Json 返回成功信息
	 * @param string $data 数据
	 * @param string $pageinfo 数据分页
	 */
	protected function ajaxReturnSuccess($data=null,$pageinfo=null) {
		$ret = array ();
		$ret ["result"] = true;
		$ret ["data"] = $data;
		$ret["pageinfo"]=$pageinfo;
		$this->ajaxReturn ( $ret, "JSON" );
		return;
	}
	
	/**
	 * 从Session获取当前登录用户信息
	 * @param string $fieldName
	 * @return mixed
	 */
	protected function getUserFromSession($fieldName=""){
		$userInfo=session ( "CUR_LOGIN_USER");
		if(is_null($userInfo)){
			$this->redirect("/Home/Index/index",null,0,"");
		}else{
			if(isValidString($fieldName) && array_key_exists($fieldName,$userInfo)){
				return $userInfo[$fieldName];
			}else{
				return $userInfo;
			}
		}
	}
	
	/**
	 * 更新当前登录用户信息
	 * @param unknown $userInfo
	 */
	protected function setUserIntoSession($userInfo){
		session("CUR_LOGIN_USER",$userInfo);
	}
	
	/**
	 * 校验用户页面操作权限
	 * @return boolean
	 */
	protected function checkUserRight(){
		$moduleKey=$this->moduleKey;
		$userName=$this->getUserFromSession ( "userName");
		if(isValidString($userName) && isValidString($moduleKey)){
			$dao=new \Think\Model();
			$sql="select 1 from sopr_module where moduleKey='%s' and moduleId in
					(select moduleId from sopr_groupmodule where groupId=
					(select groupId from sopr_user where userName='%s'))";
			$sql=sprintf($sql,$moduleKey,$userName);
			$list=$dao->query ( $sql );
			if(count($list)>0){
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取当前页面模块信息（模块对应权限）
	 * @return \Think\mixed|boolean
	 */
	protected function getModuleInfo(){
		$dao=new \Manage\Model\ModuleModel();
		$list=$dao->getModule(-1, $this->moduleKey, "", 0, false);
		if(count($list)>0)
			return $list[0];
		return false;
	}
	
	/**
	 * 写日志
	 * @param unknown $operate
	 * @param unknown $keyword
	 * @param unknown $content
	 * @param string $platform
	 * @return \Think\false
	 */
	protected function writeLog($operate, $keyword, $content,$platform="jddj"){
		$userName= $this->getUserFromSession("userName");
		if($this->soprLogDao==null){
			$this->soprLogDao=new  \Manage\Model\LogModel();
		}
		return $this->soprLogDao->addLog($userName, $platform, $this->moduleKey, $operate, $keyword, $content);
	}
	
	/**
	 * 获取登录IP
	 * @return Ambigous <string, unknown>
	 */
	protected function getClientIP() {
		$IPaddress = '';
		if (isset ( $_SERVER )) {
			if (isset ( $_SERVER ["HTTP_X_FORWARDED_FOR"] )) {
				$IPaddress = $_SERVER ["HTTP_X_FORWARDED_FOR"];
			} else if (isset ( $_SERVER ["HTTP_CLIENT_IP"] )) {
				$IPaddress = $_SERVER ["HTTP_CLIENT_IP"];
			} else {
				$IPaddress = $_SERVER ["REMOTE_ADDR"];
			}
		} else {
			if (getenv ( "HTTP_X_FORWARDED_FOR" )) {
				$IPaddress = getenv ( "HTTP_X_FORWARDED_FOR" );
			} else if (getenv ( "HTTP_CLIENT_IP" )) {
				$IPaddress = getenv ( "HTTP_CLIENT_IP" );
			} else {
				$IPaddress = getenv ( "REMOTE_ADDR" );
			}
		}
		return $IPaddress;
	}

}