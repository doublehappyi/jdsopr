<?php

namespace Common\Model;

use Think\Model;

class SoprModel extends Model {
	
	public function __construct($name='',$tablePrefix='',$connection=''){
			parent::__construct($name,$tablePrefix,$connection);
	}
	
	public function query($sql,$parse=false){
		//$sql=ConvertEncode("UTF-8","GBK//IGNORE",$sql);
		//$var=parent::query($sql,$parse);
		//return ConvertEncode("GBK","UTF-8//IGNORE",$var);
		return parent::query($sql,$parse);
	}
	
	public function execute($sql,$parse=false) {
	 	//$sql=ConvertEncode("UTF-8","GBK//IGNORE",$sql);
	 	//$var=parent::execute($sql,$parse);
	 	//return ConvertEncode("GBK","UTF-8//IGNORE",$var);
	 	return parent::execute($sql,$parse);
	}
}
