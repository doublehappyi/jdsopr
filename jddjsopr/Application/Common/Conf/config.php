<?php
return array(
		'SESSION_OPTIONS'=>array('expire'=>3600),
		//自定义配置项开始
		'IS_DEVELOP_MODE'=>true,
		'WEB_ROOT_PATH'=>'/var/www/html/sopr',
		'PUB_SHELL_PATH'=>'/data/searchdata/shell',
		'PUB_DATA_PATH'=>'/data/searchdata/operation_o2o',
		//搜索运营平台数据库（默认库）
		'DB_TYPE' => 'mysql',
		'DB_HOST' => '127.0.0.1',
		'DB_USER' => 'sopr',
		'DB_PWD' => 'sopr',
		'DB_PORT' => 3306,
		'DB_NAME' => 'sopr',
		'DB_CHARSET' => 'utf8',
);
/*return array(
		'SESSION_OPTIONS'=>array('expire'=>3600),
		//自定义配置项开始
		'IS_DEVELOP_MODE'=>false,
		'WEB_ROOT_PATH'=>'/data/html/site/sopr',
		'PUB_SHELL_PATH'=>'/data/searchdata/shell',
		'PUB_DATA_PATH'=>'/data/searchdata/operation_o2o',
		//搜索运营平台数据库（默认库）
		'DB_TYPE' => 'mysql',
		'DB_HOST' => '10.189.30.48',
		'DB_USER' => 'search_opr',
		'DB_PWD' => 'search@idc321',
		'DB_PORT' => 9029,
		'DB_NAME' => 'search_opr',
		'DB_CHARSET' => 'utf8',
);*/