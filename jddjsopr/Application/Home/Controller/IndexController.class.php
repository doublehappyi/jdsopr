<?php

namespace Home\Controller;

use Think\Controller;
use FlagShipStore;
use Home\Model\HomeModel;
use Common\Controller\SoprController;

class IndexController extends SoprController {
	
	protected $moduleKey="mainpage";
	
	public function index() {
		$loginurl=sprintf("http://%s:%s/index.php/Home/Index/login",I('server.SERVER_NAME'),I('server.SERVER_PORT'));
		$this->assign("loginurl",urlencode($loginurl));
		$this->assign("nowyear",date('Y'));
		$this->display ( "login" );
	}
	
	public function logout() {
		$this->writeLog($this::LOGTYPE_LOGOUT, "", "退出系统");
		$this->setUserIntoSession(null);
		session ( null );
		cookie('sname','',3600);
		$this->index(); 
	}
	
	public function main(){
		$userName=$this->getUserFromSession("userName");
		$dao = new \Manage\Model\ModuleModel ();
		$modulelist=$dao->getUserModule($userName);
		
		$adminRight=false;
		for($i=0;$i<count($modulelist);$i++){
			$this->assign ( "USERRIGHT_".$modulelist[$i]["moduleKey"], true);
			if($modulelist[$i]["moduleKey"]=="usermanage" || $modulelist[$i]["moduleKey"]=="modulemanage" || 
				$modulelist[$i]["moduleKey"]=="groupmanage" || $modulelist[$i]["moduleKey"]=="logmanage"){
				$adminRight=true;
			}
		}
		$this->assign ( "ADMIN_RIGHT", $adminRight);
		$this->assign ( "loginuser", $userName);
		$this->assign ( "usergroup", $this->getUserFromSession("groupName"));
		$this->assign("nowyear",date('Y'));
		$this->display ( "main" );
	}
	
	public function login() {
		$username = trim ( I ( "username" ) );
		$password = trim ( I ( "password" ) );
		$verifycode = trim ( I ( "verifycode" ));
		$ticket=trim ( I ("ticket"));
		if($ticket!=""){
			//OA登录
			$mySoap = new \SoapClient("http://passport.oa.com/services/passportservice.asmx?WSDL");
			$soapResult = $mySoap -> DecryptTicket(array("encryptedTicket" => $_GET['ticket']));
			$username = $soapResult -> DecryptTicketResult -> LoginName;	
			$dao = new \Manage\Model\UserModel ();
			$userlist = $dao->getUser($username, -1, -1,false);
			if (count ( $userlist ) == 0) {
				return $this->errlogin ( "用户名不存在" );
			}
			if($userlist [0] ["isDelete"]!=0){
				return $this->errlogin ( "用户名已删除，请联系管理员" );
			}
		}
		else{
			if (isNoValidString($username)) {
				return $this->errlogin ( "请输入登录用户名" );
			}
			if (isNoValidString($password)) {
				return $this->errlogin ( "请输入登录密码" );
			}
			if (isNoValidString($verifycode)) {
				return $this->errlogin ( "请输入登录校验码" );
			}
			$verify = new \Think\Verify ();
			if ($verify->check ( $verifycode ) == false) {
				return $this->errlogin ( "登录校验码错误" );
			}
			$dao = new \Manage\Model\UserModel ();
			$userlist = $dao->getUser($username, -1, -1,false);
			if (count ( $userlist ) == 0) {
				return $this->errlogin ( "用户名不存在" );
			}
			if($userlist [0] ["isDelete"]!=0){
				return $this->errlogin ( "用户名已删除，请联系管理员" );
			}
			if ($userlist [0] ["passWord"] != md5($password)) {
				return $this->errlogin ( "密码错误" );
			}
		}
		$dao->updateLoginTime($userlist[0]["userName"]);
		$this->setUserIntoSession($userlist[0]);
		$this->writeLog($this::LOGTYPE_LOGIN, $this->getClientIP(), "登录系统");
		$loginmsg=sprintf("亲爱的 %s ,您上次登录时间是 %s ",$userlist[0]["userName"],$userlist[0]["lastLoginTime"]);
		if($userlist[0]["passWord"] == md5("123456")){
			$loginmsg=$loginmsg.",<font color=\"red\">请尽快修改您的登录密码.</font>";
		}
		cookie('sname',$userlist[0]["userName"],3600);
		$this->success($loginmsg,"main",2);
	}
	
	private function errlogin($errmsg) {
		$this->assign ( "errmsg", $errmsg );
		$this->index(); 
	}
	
	// 生成验证码
	public function verify() {
		$config =    array(
				'fontSize'  =>30,
				'useCurve'	=>false,
				'useNoise'	=>true,
				'codeSet'   =>'2345789ABDEFHJKMNPRTUVXY',
				'length'    =>5,
		);
		$verify = new \Think\Verify ($config);
    	$verify->entry();
    }
	
}