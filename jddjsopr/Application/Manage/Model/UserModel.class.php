<?php

namespace Manage\Model;

use Common\Model\SoprModel;

class UserModel extends SoprModel {
	
	protected  $trueTableName="sopr_user";
	
	public function getUser($userName, $groupId, $isDelete,$islike=true) {
		$sql = "select u.*,g.groupName from sopr_user u left join sopr_group g on u.groupId=g.groupId where 1=1 ";
		if ($userName != "") {
			if($islike===true){
				$sql = $sql . sprintf ( "and u.userName like '%%%s%%' ", mysql_escape_string ( $userName ) );
			}else{
				$sql = $sql . sprintf ( "and u.userName='%s' ", mysql_escape_string ( $userName ) );
			}
		}
		if ( $groupId >= 0) {
			$sql = $sql . sprintf ( "and u.groupId=%d ", $groupId );
		}
		if ( $isDelete >= 0) {
			$sql = $sql . sprintf ( "and u.isDelete=%d ", $isDelete );
		}
		$sql=$sql." order by userId ";
		return $this->query ( $sql );
	}
	
	public function delUserByName($userName) {
		if ($userName !="") {
			$sqlformat = "update sopr_user set isDelete=1 where userName='%s'";
			return $this->execute ( sprintf ( $sqlformat,mysql_escape_string ( $userName ) ) );
		}
		return false;
	}
	
	public function addUser( $userName, $passWord, $groupId, $isDelete) {
		if ($userName != "" && $passWord != "" && $groupId >= 0 && $isDelete >= 0) {
			$sqlformat = "insert into sopr_user(userName,passWord,groupId,createTime,isDelete) values('%s','%s',%d,'%s',%d)";
			$sql=sprintf($sqlformat,mysql_escape_string($userName),mysql_escape_string($passWord),$groupId,date("Y-m-d H:i:s"),$isDelete);
			return $this->execute ( $sql );
		}
		return false;
	}
	
	public function saveUser($userName, $groupId, $isDelete)
	{
		if ($userName != "" && $groupId >= 0 && $isDelete >= 0) {
			$sqlformat = "update sopr_user set groupId=%d,isDelete=%d where userName='%s'";
			$sql=sprintf($sqlformat,$groupId,$isDelete,mysql_escape_string($userName));
			return $this->execute ( $sql );
		}
		return false;
	}
	
	public function updatePassWord($username, $newpwd) {
		$sqlformat = "update sopr_user set passWord='%s' where userName='%s'";
		return $this->execute ( sprintf ( $sqlformat, mysql_escape_string ( $newpwd ), mysql_escape_string ( $username ) ) );
	}
	
	public function updateLoginTime($username){
		$sqlformat = "update sopr_user set lastLoginTime='%s' where userName='%s'";
		return $this->execute ( sprintf ( $sqlformat, date("Y-m-d H:i:s"), mysql_escape_string ( $username ) ) );
	}
	
}
