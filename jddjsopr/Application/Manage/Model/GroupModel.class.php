<?php

namespace Manage\Model;

use Common\Model\SoprModel;

class GroupModel extends SoprModel {
	protected  $trueTableName="sopr_group";
	
	public function getGroup($groupid, $groupname, $isDelete,$islike=true) {
		$sql = "select * from sopr_group where 1=1 ";
		if ($groupid>=0) {
			$sql = $sql . sprintf ( " and groupId=%d ", $groupid );
		}
		if ($groupname != "") {
			if($islike===true){
				$sql = $sql . sprintf ( " and groupName like '%%%s%%' ", mysql_escape_string ( $groupname ) );
			}else{
				$sql = $sql . sprintf ( " and groupName='%s' ", mysql_escape_string ( $groupname ) );
			}
		}
		if ($isDelete >= 0) {
			$sql = $sql . sprintf ( "and isDelete=%d ", $isDelete );
		}
		$sql = $sql . "order by groupId asc ";
		return $this->query ( $sql );
	}
	
	public function getGroupModule($groupid){
		if($groupid>=0){
			$sqlformat = "select * from sopr_module where isDelete=0 and moduleId in
					(select moduleId from sopr_groupmodule where groupId=%d) order by sortNo asc,moduleId asc";
			$sql=sprintf($sqlformat,$groupid);
			return $this->query ( $sql );
		}
		return array();
	}
	
	public function getGroupModuleAll($groupid){
		if($groupid>=0){
			$sqlformat = "select m.*,IFNULL(gm.moduleId,-1) as ischecked from sopr_module m left join 
					(select * from sopr_groupmodule where groupId=%d) gm on m.moduleId=gm.moduleId where m.isDelete=0 order by m.sortNo asc,m.moduleId asc";
			$sql=sprintf($sqlformat,$groupid);
			return $this->query ( $sql );
		}else{
			$sql = "select m.*,-1 as ischecked from sopr_module m where m.isDelete=0 order by m.sortNo asc,m.moduleId";
			return $this->query ( $sql );
		}
	}
	
	public function getGroupUser($groupid){
		if($groupid>=0){
			$sqlformat = "select * from sopr_user where isDelete=0 and groupId=%d order by userId";
			$sql=sprintf($sqlformat,$groupid);
			return $this->query ( $sql );
		}
		return array();
	}
	
	public function delGroup($groupId){
		if($groupId>=0){
			try{
				$this->startTrans();
				$sql = sprintf("update sopr_group set isDelete=1 where groupId=%d ",$groupId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$sql = sprintf("delete from sopr_groupmodule where groupId=%d ",$groupId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
		return false;
	}
	
	public function addGroup($groupName,$isDelete,$modulearray){
		if($groupName!="" && $isDelete>=0){
			$groupName=mysql_escape_string($groupName);
			try{
				$this->startTrans();
				$sql = sprintf("insert into sopr_group(groupName,createTime,isDelete) values('%s','%s',%d) ",$groupName,date("Y-m-d H:i:s"),$isDelete);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$tempgrouplist=$this->getGroup(-1,$groupName,$isDelete,false);
				if(count($tempgrouplist)<=0){
					$this->rollback();
					return false;
				}
				if($tempgrouplist[0]["isDelete"]==0){
					for($i=0; $i<count($modulearray);$i++){
						$sql = sprintf("insert into sopr_groupmodule(groupId,moduleId) values(%d,%d) ",$tempgrouplist[0]["groupId"], $modulearray[$i]);
						if($this->execute ( $sql )===false){
							$this->rollback();
							return false;
						}
					}
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
		return false;
	}
	
	public function saveGroup($groupId,$groupName,$isDelete,$modulearray){
		if($groupName!="" && $isDelete>=0){
			$groupName=mysql_escape_string($groupName);
			try{
				$this->startTrans();
				$sql = sprintf("update sopr_group set groupName='%s',isDelete=%d where groupId=%d",$groupName,$isDelete,$groupId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$sql = sprintf("delete from sopr_groupmodule where groupId=%d",$groupId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				if($isDelete==0){
					for($i=0; $i<count($modulearray);$i++){
						$sql = sprintf("insert into sopr_groupmodule(groupId,moduleId) values(%d,%d) ",$groupId, $modulearray[$i]);
						if($this->execute ( $sql )===false){
							$this->rollback();
							return false;
						}
					}
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
		return false;
	}
}
