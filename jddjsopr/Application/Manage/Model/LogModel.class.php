<?php

namespace Manage\Model;

use Common\Model\SoprModel;

class LogModel extends SoprModel {
	
	protected  $trueTableName="sopr_log";
	
	private function makeSelectSQL($userName,$platform,$module,$operate,$keyword,$start,$end,$islike){
	$sql = "select l.*,m.moduleTitle from sopr_log l left join sopr_module m on l.module=m.moduleKey where 1=1 ";
		if ($userName!="") {
			$sql = $sql . sprintf ( " and l.userName='%s' ", mysql_escape_string($userName));
		}
		if ($platform!="") {
			$sql = $sql . sprintf ( " and l.platform='%s' ", mysql_escape_string ( $platform ));
		}
		if ($module!="") {
			$sql = $sql . sprintf ( " and l.module='%s' ", mysql_escape_string ( $module ));
		}
		if ( $operate !="") {
			$sql = $sql . sprintf ( " and l.operate='%s' ", mysql_escape_string($operate));
		}
		if($keyword!=""){
			if($islike){
				$sql = $sql . sprintf ( " and l.keyword like '%%%s%%' ", mysql_escape_string($keyword) );
			}else{
				$sql = $sql . sprintf ( " and l.keyword='%s' ", mysql_escape_string($keyword) );
			}
		}
		if($start!=""){
			$sql = $sql . sprintf ( " and l.createTime>='%s' ", mysql_escape_string ( $start ) );
		}
		if($end!=""){
			$sql = $sql . sprintf ( " and l.createTime<='%s' ", mysql_escape_string ( $end ) );
		}
		return $sql;
	}
	
	public function getLogPageInfo($userName,$platform,$module,$operate,$keyword,$start,$end,$islike,$page,$pagesize) {
		$sql = sprintf("select count(*) as num from (%s) t",$this->makeSelectSQL($userName,$platform,$module,$operate,$keyword,$start,$end,$islike));
		$list = $this->query ( $sql );
		$num=intval($list[0]["num"]);
		$pageinfo['TotalNum']=$num;
		$pageinfo['CurrentPage']=$page;
		$pageinfo['PageSize']=$pagesize;
		$temp=intval($num/$pagesize);
		if(0!=($num%$pagesize))
		{
			$temp+=1;
		}
		$pageinfo['TotalPage']=$temp;
	
		return $pageinfo;
	}
	
	public function getLog($userName,$platform,$module,$operate,$keyword,$start,$end,$islike,$page,$pagesize){
		$sql=$this->makeSelectSQL($userName,$platform,$module,$operate,$keyword,$start,$end,$islike);
		$sql=$sql.sprintf(" order by l.createTime desc limit %d,%d ", $page*$pagesize, $pagesize);
		return $this->query($sql);
	}
	
	public  function getLogOperate(){
		$sql = "select distinct operate as operate from sopr_log ";
		return $this->query ( $sql );
	}
	
	public function addLog($userName,$platform,$module,$operate,$keyword,$content){
		$sqlformat="insert into sopr_log(userName,platform,module,operate,keyword,content,createTime) values('%s','%s','%s','%s','%s','%s','%s')";
		$sql=sprintf($sqlformat,mysql_escape_string($userName),mysql_escape_string($platform),mysql_escape_string($module),$operate,
				mysql_escape_string($keyword),mysql_escape_string($content),date("Y-m-d H:i:s"));
		return $this->execute($sql);
	}
}
