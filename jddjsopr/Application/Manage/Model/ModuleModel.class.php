<?php

namespace Manage\Model;

use Common\Model\SoprModel;

class ModuleModel extends SoprModel {
	
	protected  $trueTableName="sopr_module";
	
	public function getModule($moduleId,$moduleKey,$moduleTitle,$isDelete,$islike=true){
		$sql = "select * from sopr_module where 1=1 ";
		if ($moduleId>=0) {
			$sql = $sql . sprintf ( " and moduleId=%d ", $moduleId );
		}
		if ($moduleKey!="") {
			if($islike===true){
				$sql = $sql . sprintf ( " and moduleKey like '%%%s%%' ", mysql_escape_string ( $moduleKey ) );
			}else{
				$sql = $sql . sprintf ( " and moduleKey='%s' ", mysql_escape_string ( $moduleKey ) );
			}
		}
		if ($moduleTitle!="") {
			if($islike===true){
				$sql = $sql . sprintf ( " and moduleTitle like '%%%s%%' ", mysql_escape_string ( $moduleTitle ) );
			}else{
				$sql = $sql . sprintf ( " and moduleTitle='%s' ", mysql_escape_string ( $moduleTitle ) );
			}
		}
		if ( $isDelete >= 0) {
			$sql = $sql . sprintf ( " and isDelete=%d ", $isDelete );
		}
		$sql=$sql." order by sortNo asc,moduleId asc";
		return $this->query ( $sql );
	}
	
	public function getUserModule($userName){
		$sqlformat="select * from sopr_module where moduleId in(select moduleId from sopr_groupmodule where groupId=
				(select groupId from sopr_user where userName='%s')) order by sortNo asc,moduleId asc";
		return  $this->query ( sprintf($sqlformat,mysql_escape_string($userName)));
	}
	
	public function delModule($moduleId) {
		if($moduleId>=0){
			try{
				$this->startTrans();
				$sql = sprintf("update sopr_module set isDelete=1 where moduleId=%d ",$moduleId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$sql = sprintf("delete from sopr_groupmodule where moduleId=%d ",$moduleId);
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
		return false;
	}
	
	public function addModule( $moduleKey,$moduleTitle,$dataFilePath,$syncFilePath,$srcUrl,$isDelete,$sortNo) {
		if ($moduleKey != "" && $moduleTitle != "" && $isDelete >= 0) {
			$sqlformat = "insert into sopr_module(moduleKey,moduleTitle,dataFilePath,syncFilePath,srcUrl,createTime,isDelete,sortNo) 
					values('%s','%s','%s','%s','%s','%s',%d,%d)";
			$sql=sprintf($sqlformat,mysql_escape_string($moduleKey),mysql_escape_string($moduleTitle),mysql_escape_string($dataFilePath),
					mysql_escape_string($syncFilePath),mysql_escape_string($srcUrl),date("Y-m-d H:i:s"),$isDelete,$sortNo);
			return $this->execute ( $sql );
		}
		return false;
	}
	
	public function saveModule($moduleKey,$moduleTitle,$dataFilePath,$syncFilePath,$srcUrl,$isDelete,$sortNo)
	{
		if ($moduleKey != "" && $moduleTitle != "" && $isDelete >= 0 && $sortNo>=0) {
			try{
				$this->startTrans();
				$sqlformat = "update sopr_module set moduleTitle='%s',dataFilePath='%s',syncFilePath='%s',srcUrl='%s',isDelete=%d,sortNo=%d where moduleKey='%s'";
				$sql=sprintf($sqlformat,mysql_escape_string($moduleTitle),mysql_escape_string($dataFilePath),mysql_escape_string($syncFilePath),
						mysql_escape_string($srcUrl),$isDelete,$sortNo,mysql_escape_string($moduleKey));
				if($this->execute ( $sql )===false){
					$this->rollback();
					return false;
				}
				if($isDelete!=0){
					$sql = sprintf("delete from sopr_groupmodule where moduleId in(select moduleId from sopr_module where moduleKey='%s') ",
							mysql_escape_string($moduleKey));
					if($this->execute ( $sql )===false){
						$this->rollback();
						return false;
					}
				}
				$this->commit();
				return true;
			}catch (\Exception $e){
				$this->rollback();
				return  false;
			}
		}
		return false;
	}
}
