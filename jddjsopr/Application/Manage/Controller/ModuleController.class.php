<?php

namespace Manage\Controller;

use Common\Controller\SoprController;

class ModuleController extends SoprController {
	
	protected $moduleKey="modulemanage";
	
	public function index() {
		$this->display ( "moduleManage" );
	}
	
	public function editModule()
	{
		$moduleId=trim(I("moduleId"));
		if(isPositiveNumeric($moduleId)){
			$moduleId=intval($moduleId);
			$dao=new \Manage\Model\ModuleModel ();
			$modulelist=$dao->getModule($moduleId, "","", -1,false);
			if(count($modulelist)>0){
				$this->assign ( "moduleObj", $modulelist[0] );
			}
		}
		$this->display ( "editModule" );
	}
	
	public function getModuleList()
	{
		$moduleKey = trim ( I ( "moduleKey" ) );
		$moduleTitle=trim ( I ( "moduleTitle" ) );
		$isDelete=trim ( I ( "isDelete" ) );
	
		if(isPositiveNumeric($isDelete))
		{
			$isDelete=intval($isDelete);
		}
		else
		{
			$isDelete=-1;
		}
		$dao = new \Manage\Model\ModuleModel ();
		$data=$dao->getModule(-1,$moduleKey, $moduleTitle, $isDelete);
		$this->ajaxReturnSuccess( $data);
		return;
	}

	public function delModule()
	{
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		
		$moduleId = trim ( I ( "moduleId" ) );
		if(isPositiveNumeric($moduleId)){
			$moduleId=intval($moduleId);
			$dao = new \Manage\Model\ModuleModel ();
			$ret=$dao->delModule($moduleId);
			if($ret===false )
			{
				return $this->ajaxReturnError ( "删除模块失败" );
			}
			$this->writeLog($this::LOGTYPE_DELETE, "", sprintf("moduleId:%d",$moduleId));
			return $this->ajaxReturnSuccess ( );
			
		}
		return $this->ajaxReturnError ( "模块ID无效" );
	}

	public function saveModule(){
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		
		$moduleId = trim ( I ( "moduleId" ));
		$moduleKey = trim ( I ( "moduleKey"));
		$moduleTitle=trim ( I ( "moduleTitle" ) );
		$sortNo=trim ( I ( "sortNo" ) );
		$dataFilePath = trim ( I ( "dataFilePath" ));
		$syncFilePath = trim ( I ( "syncFilePath"));
		$srcUrl=trim ( I ( "srcUrl" ) );
		$isDelete=trim ( I ( "isDelete" ));
		if(isNoValidString($moduleKey))
		{
			return $this->ajaxReturnError ( "模块标识无效" );
		}
		if(isNoValidString($moduleTitle))
		{
			return $this->ajaxReturnError ( "模块标题无效" );
		}
		if(!isPositiveNumeric($isDelete))
		{
			return $this->ajaxReturnError ( "模块是否删除无效" );
		}
		if(!isPositiveNumeric($sortNo)){
			return $this->ajaxReturnError ( "顺序号无效" );
		}
		$isDelete=intval($isDelete);
		$sortNo=intval($sortNo);
		$dao = new \Manage\Model\ModuleModel ();
		if($moduleId=="")
		{
			//新增
			$templist=$dao->getModule(-1, $moduleKey, "", -1,false);
			if(count($templist)>0){
				return $this->ajaxReturnError ( "模块标识已经存在" );
			}
			$ret=$dao->addModule($moduleKey, $moduleTitle, $dataFilePath, $syncFilePath, $srcUrl, $isDelete,$sortNo);
			if($ret===false )	{
				return $this->ajaxReturnError ( "新增模块失败" );
			}
			$this->writeLog($this::LOGTYPE_NEW, "", sprintf("moduleKey:%s,moduleTitle:%s,dataFilePath:%s,syncFilePath:%s,srcUrl:%s,isDelete:%d,sortNo:%d",
					$moduleKey,$moduleTitle,$dataFilePath, $syncFilePath, $srcUrl,$isDelete,$sortNo));
			return $this->ajaxReturnSuccess();
		}
		else {
			//修改
			$ret=$dao->saveModule($moduleKey, $moduleTitle, $dataFilePath, $syncFilePath, $srcUrl, $isDelete,$sortNo);
			if($ret=== false ){
				return $this->ajaxReturnError ( "修改模块失败" );
			}
			$this->writeLog($this::LOGTYPE_MODIFY, "", sprintf("moduleKey:%s,moduleTitle:%s,dataFilePath:%s,syncFilePath:%s,srcUrl:%s,isDelete:%d,sortNo:%d",
					$moduleKey,$moduleTitle,$dataFilePath, $syncFilePath, $srcUrl,$isDelete,$sortNo));
			return $this->ajaxReturnSuccess();
		}
	}
}