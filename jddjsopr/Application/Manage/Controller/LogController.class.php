<?php

namespace Manage\Controller;

use Common\Controller\SoprController;

class LogController extends SoprController {
	
	protected $moduleKey="logmanage";
	
	public function index(){
		$dao = new \Manage\Model\LogModel();
		$operatelist=$dao->getLogOperate();
		$dao=new \Manage\Model\ModuleModel();
		$modulelist=$dao->getModule(-1, "", "", 0);
		$this->assign ( "haslogmanage", true);
		$this->assign ( "userName", $userName );
		$this->assign ( "operatelist", $operatelist );
		$this->assign ( "modulelist",$modulelist);
		$this->display ( "index" );
	}
	
	public function viewUserLog(){
		$userName=$this->getUserFromSession("userName");
		$dao = new \Manage\Model\LogModel();
		$operatelist=$dao->getLogOperate();
		$dao=new \Manage\Model\ModuleModel();
		$modulelist=$dao->getModule(-1, "", "", 0);
		$this->assign ( "userName", $userName );
		$this->assign ( "operatelist", $operatelist );
		$this->assign("modulelist",$modulelist);
		$this->display ( "index" );
	}
	
	public function getLogList(){
		$userName=trim(I("userName"));
		$module=trim(I("module"));
		$operate=trim(I("operate"));
		$keyword=trim(I("keyword"));
		$start=trim(I("starttime"));
		$end=trim(I("endtime"));
		$page=trim(I("page"));
		$pagesize=trim(I("pagesize"));
		$page=isPositiveNumeric($page)?intval($page):0;
		$pagesize=isPositiveNumeric($pagesize)?intval($pagesize):25;
		
		$dao = new \Manage\Model\LogModel();
		$pageinfo=$dao->getLogPageInfo($userName, "", $module, $operate, $keyword, $start, $end, true, $page, $pagesize);
		$data=$dao->getLog($userName, "", $module, $operate, $keyword, $start, $end, true, $page, $pagesize);
		return $this->ajaxReturnSuccess($data,$pageinfo);
		
	}
}