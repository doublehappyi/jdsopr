<?php
namespace Manage\Controller;
use Think\Controller;
use Common\Controller\SoprController;

class GroupController extends SoprController {
	
	protected $moduleKey="groupmanage";
	
    public function index(){
       $this->display("groupManage");
    }
    
    public function viewGroup(){
    	$groupId = trim ( I ( "groupId" ) );
    	if(isPositiveNumeric($groupId)){
    		$groupId=intval($groupId);
    		$dao = new \Manage\Model\GroupModel ();
    		$grouplist=$dao->getGroup($groupId, "", -1);
    		if(count($grouplist)>0){
    			$userlist=$dao->getGroupUser($groupId);
    			$modulelist=$dao->getGroupModule($groupId);
    			$this->assign("group",$grouplist[0]);
    			$this->assign("groupuser",$userlist);
    			$this->assign("groupmodule",$modulelist);
    			$this->display("viewGroup");
    		}
    	}
    	return;
    }
    
    public function editGroup(){
    	$groupId = trim ( I ( "groupId" ) );
    	$dao = new \Manage\Model\GroupModel ();
    	if(isPositiveNumeric($groupId)){
    		$groupId=intval($groupId);
    		$grouplist=$dao->getGroup($groupId, "", -1);
    		if(count($grouplist)>0){
    			$this->assign("group",$grouplist[0]);
    		}
    	}else{
    		$groupId=-1;
    	}
    	$modulelist=$dao->getGroupModuleAll($groupId);
    	$this->assign("groupmodule",$modulelist);
    	$this->display("editGroup");
    	return;
    }
    
    public function getGroupList()
    {
    	$groupName = trim ( I ( "groupName" ) );
    	$isDelete=trim ( I ( "isDelete" ) );
    
    	if(isPositiveNumeric($isDelete))
    	{
    		$isDelete=intval($isDelete);
    	}
    	else
    	{
    		$isDelete=-1;
    	}
    	$dao = new \Manage\Model\GroupModel ();
    	$data=$dao->getGroup(-1, $groupName, $isDelete);
    	$this->ajaxReturnSuccess ( $data, "JSON" );
    	return;
    }
    
    public function delGroup(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$groupId = trim ( I ( "groupId" ) );
    	if(isPositiveNumeric($groupId)){
    		$groupId=intval($groupId);
    		$dao = new \Manage\Model\GroupModel ();
    		$userlist=$dao->getGroupUser($groupId);
    		if(count($userlist)>0){
    			return $this->ajaxReturnError("该角色下有用户，不能删除");
    		}
    		if($dao->delGroup($groupId)===false){
    			return $this->ajaxReturnError("删除角色失败");
    		}
    		$this->writeLog($this::LOGTYPE_DELETE, "", sprintf("groupId:%d",$groupId));
    		return $this->ajaxReturnSuccess();
    	}
    	return $this->ajaxReturnError("角色ID无效");
    }
    
    public function saveGroup(){
    	if($this->checkUserRight()==false){
    		return $this->ajaxReturnError ( "没有操作权限");
    	}
    	
    	$groupId = trim ( I ( "groupId" ));
    	$moduleId = trim ( I ( "moduleId" ));
    	$groupName = trim ( I ( "groupName" ));
    	$isDelete=trim ( I ( "isDelete" ));
    	$moduleIdArray=array();
    	
    	if(isNoValidString($groupName))
    	{
    		return $this->ajaxReturnError ( "角色名无效" );
    	}
    	if(!isPositiveNumeric($isDelete))
    	{
    		return $this->ajaxReturnError ( "角色是否删除无效" );
    	}
    	$isDelete=intval($isDelete);
    	if(isValidString($groupId) && !isPositiveNumeric($groupId)){
    		return $this->ajaxReturnError ( "角色ID无效" );
    	}
    	
    	if($isDelete==0){
    		$templist=explode("_", $moduleId);
    		for($i=0;$i<count($templist);$i++){
    			if(isPositiveNumeric($templist[$i])){
    				array_push($moduleIdArray,intval($templist[$i]));
    			}
    		}
    	}
    	
    	$dao = new \Manage\Model\GroupModel ();
    	if($groupId==""){
    		//新增
    		$templist=$dao->getGroup(-1, $groupName, 0,false);
    		if(count($templist)>0){
    			return $this->ajaxReturnError ( "角色名已经存在" );
    		}
    		if($dao->addGroup($groupName, $isDelete, $moduleIdArray)===false){
    			return $this->ajaxReturnError ( "新增角色失败" );
    		}else{
    			$this->writeLog($this::LOGTYPE_NEW, "", sprintf("groupName:%s",$groupName));
    			return $this->ajaxReturnSuccess ();
    		}
    	}else{
    		//修改
    		$groupId=intval($groupId);
    		$templist=$dao->getGroup($groupId, "", -1);
    		if(count($templist)<=0){
    			return $this->ajaxReturnError ( "所修改的角色不存在" );
    		}
    		if($isDelete!=0){
    			$templist=$dao->getGroupUser($groupId);
    			if(count($templist)>0){
    				return $this->ajaxReturnError ( "所修改的角色存在用户，不能删除" );
    			}
    		}
    		$templist=$dao->getGroup(-1, $groupName, 0,false);
    		for($i=0;$i<count($templist);$i++){
    			if($templist[$i]["groupId"]!=$groupId){
    				return $this->ajaxReturnError ( "角色名已经存在" );
    			}
    		}
    		if($dao->saveGroup($groupId,$groupName, $isDelete, $moduleIdArray)===false){
    			return $this->ajaxReturnError ( "修改角色失败" );
    		}else{
    			$this->writeLog($this::LOGTYPE_MODIFY, "", sprintf("groupId:%d,groupName:%s,isDelete:%d",$groupId,$groupName,$isDelete));
    			return $this->ajaxReturnSuccess ();
    		}
    	}
    }
    
}