<?php

namespace Manage\Controller;

use Common\Controller\SoprController;

class UserController extends SoprController {
	
	protected $moduleKey="usermanage";
	
	public function index() {
		$this->display ( "userManage" );
	}
	
	public function changePassword() {
		$this->display ( "changePassword" );
	}
	
	public function editUser()
	{
		$dao = new \Manage\Model\GroupModel ();
		$grouplist=$dao->getGroup ( -1, "", 0 );
		$this->assign ( "grouplist", $grouplist );
		
		$userName=trim(I("userName"));
		if(isValidString($userName)){
				$dao=new \Manage\Model\UserModel ();
				$userlist=$dao->getUser($userName, -1, -1,false);
				if(count($userlist)>0){
					$this->assign ( "userObj", $userlist[0] );
				}
		}
		$this->display ( "editUser" );
	}
	
	public function viewUserModule(){
		$userName=$this->getUserFromSession("userName");
		$dao = new \Manage\Model\ModuleModel ();
		$modulelist=$dao->getUserModule($userName);
		$this->assign ( "modulelist", $modulelist );
		$this->display ( "viewUserModule" );
	}
	
	public function getUserList()
	{
		$userName = trim ( I ( "userName" ) );
		$groupId=trim ( I ( "groupId" ) );
		$isDelete=trim ( I ( "isDelete" ) );
	
		if(isPositiveNumeric($groupId))
		{
			$groupId=intval($groupId);
		}
		else
		{
			$groupId=-1;
		}
		if(isPositiveNumeric($isDelete))
		{
			$isDelete=intval($isDelete);
		}
		else
		{
			$isDelete=-1;
		}
		$dao = new \Manage\Model\UserModel ();
		$data=$dao->getUser($userName, $groupId, $isDelete);
		$this->ajaxReturnSuccess( $data);
		return;
	}
	
	public function getGroupList() {
		$dao = new \Manage\Model\GroupModel ();
		$data = $dao->getGroup ( -1, "", 0 );
		$this->ajaxReturnSuccess( $data);
		return;
	}

	public function savePassword() {
		$username = $this->getUserFromSession("userName");
		$oldpassword = trim ( I ( "oldpassword" ) );
		$newpassword = trim ( I ( "newpassword" ) );
	
		if (isNoValidString($oldpassword )) {
			return $this->ajaxReturnError ( "原密码不能为空" );
		}
		if (isNoValidString($newpassword )) {
			return $this->ajaxReturnError ( "新密码不能为空" );
		}
		$dao = new \Manage\Model\UserModel ();
		$userlist = $dao->getUser($username, -1, -1, false);
		if (count ( $userlist ) == 0) {
			return $this->ajaxReturnError ( "用户不存在" );
		}
		if ($userlist [0] ["passWord"] != md5($oldpassword)) {
			return $this->ajaxReturnError ( "原始密码错误" );
		}
		$ret = $dao->updatePassWord ( $username, md5($newpassword));
		if ($ret === false ) {
			return $this->ajaxReturnError ( "密码修改失败" );
		}
		$this->writeLog($this::LOGTYPE_MODIFY, "", "修改密码");
		return $this->ajaxReturnSuccess ( );
	}
	
	public function resetPassword()
	{
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		
		$userName = trim ( I ( "userName" ) );
		$dao = new \Manage\Model\UserModel ();
		$ret=$dao->updatePassWord($userName,md5("123456"));
		if($ret===false)
		{
			return $this->ajaxReturnError ( "重置用户密码失败");
		}
		$this->writeLog($this::LOGTYPE_MODIFY, "", sprintf("重置密码:userName:%s",$userName));
		return $this->ajaxReturnSuccess ( );
	}
	
	public function delUser()
	{
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		
		$userName = trim ( I ( "userName" ) );
		$dao = new \Manage\Model\UserModel ();
		$ret=$dao->delUserByName($userName);
		if($ret===false )
		{
			return $this->ajaxReturnError ( "删除用户失败" );
		}
		$this->writeLog($this::LOGTYPE_DELETE, $userName, sprintf("userName:%s",$userName));
		return $this->ajaxReturnSuccess ( );
	}
	
	public function saveUser()
	{
		if($this->checkUserRight()==false){
			return $this->ajaxReturnError ( "没有操作权限");
		}
		
		$userId = trim ( I ( "userId" ));
		$userName = trim ( I ( "userName"));
		$groupId=trim ( I ( "groupId" ) );
		$isDelete=trim ( I ( "isDelete" ));
		
		if(isNoValidString($userName))
		{
			return $this->ajaxReturnError ( "用户名无效" );
		}
		if(!isPositiveNumeric($groupId))
		{
			return $this->ajaxReturnError ( "用户角色无效" );
		}
		$groupId=intval($groupId);
		if(!isPositiveNumeric($isDelete))
		{
			return $this->ajaxReturnError ( "用户是否删除无效" );
		}
		$isDelete=intval($isDelete);
		$dao = new \Manage\Model\UserModel ();
		if($userId=="")
		{
			//新增
			$templist=$dao->getUser($userName, -1, -1,false);
			if(count($templist)>0){
				return $this->ajaxReturnError ( "用户名已经存在" );
			}
			$ret=$dao->addUser($userName, md5("123456"), $groupId, $isDelete);
			if($ret===false )	{
				return $this->ajaxReturnError ( "新增用户失败" );
			}
			$this->writeLog($this::LOGTYPE_NEW, $userName, sprintf("userName:%s,groupId:%d",$userName,$groupId));
			return $this->ajaxReturnSuccess();
		}
		else {
			//修改
			$ret=$dao->saveUser($userName, $groupId, $isDelete);
			if($ret=== false ){
				return $this->ajaxReturnError ( "修改用户失败" );
			}
			$this->writeLog($this::LOGTYPE_MODIFY, $userName, sprintf("userName:%s,groupId:%d,isDelete:%d",$userName,$groupId,$isDelete));
			return $this->ajaxReturnSuccess();
		}
	}
	
}