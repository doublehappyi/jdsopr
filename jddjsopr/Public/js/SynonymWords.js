(function(){
    angular.module("sopr", ['ngAnimate', 'ui.bootstrap']);
    var app = angular.module('sopr');

    app.controller('soprCtrl', ['$scope', '$http', '$uibModal', '$log', '$timeout',function($scope, $http, $uibModal, $log, $timeout){
        //$scope.data = [];
        //$scope.pageinfo = {TotalNum:0, TotalPage:0, CurrentPage:0, PageSize:0};
        //$scope.keyword = '';
        //$scope.currKeyword = '';
        //
        //$scope.query = function(){
        //    $scope.currKeyword = $scope.keyword;
        //    $scope.getSynonymWords($scope.currKeyword, 0);
        //};








        $scope.items = ['item1', 'item2', 'item3'];
        $scope.animationsEnabled = true;
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };









        //$scope.getSynonymWords = function(keyword, page){
        //    var url = "/index.php/Search/SynonymWords/getSynonymWords?page="+page + "&keyword="+encodeURIComponent(keyword);
        //    $http.get(url).then(function(data){
        //        $scope.data = data.data.data;
        //        $scope.pageinfo = data.data.pageinfo;
        //    });
        //};
        //
        //$scope.getPrevPage = function(){
        //    if(($scope.pageinfo.CurrentPage - 1) < 0 ){
        //        alert("没有上一页了");
        //        return;
        //    }
        //    $scope.getSynonymWords($scope.currKeyword, $scope.pageinfo.CurrentPage - 1);
        //};
        //
        //$scope.getNextPage = function(){
        //    if(($scope.pageinfo.CurrentPage + 1) > ($scope.pageinfo.TotalPage - 1)){
        //        alert("没有下一页了");
        //        return;
        //    }
        //    $scope.getSynonymWords($scope.currKeyword, $scope.pageinfo.CurrentPage + 1);
        //};
        //
        //$scope.getFirstPage = function(){
        //    $scope.getSynonymWords($scope.currKeyword, 0);
        //};
        //
        //$scope.getLastPage = function(){
        //    $scope.getSynonymWords($scope.currKeyword, $scope.pageinfo.TotalPage - 1);
        //};
    }]);


    app.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items) {

        $scope.items = items;
        $scope.selected = {
            item: $scope.items[0]
        };

        $scope.ok = function () {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
})();