/*
 ***************************************************************************************************************************************
 * 封装共用函数
 ***************************************************************************************************************************************
 */
function GetXmlHttpRequest()
{
	var xmlHttp=null;
	try
	{
		//Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		var msxmls = new Array('Msxml2.XMLHTTP','Microsoft.XMLHTTP');
		for (var i = 0; i < msxmls.length; i++){
			try {
				xmlHttp= new ActiveXObject(msxmls[i]);
				break;
			} catch (e) {}
		}
	}
	if(xmlHttp==null)
	{
		alert ("Browser does not support XMLHTTPRequest");
		return false;
	}
	return xmlHttp;
}

/*Ajax调用服务接口*/
function AjaxCallServer(url,param,callbackfun)
{
	var xmlHttp=GetXmlHttpRequest();
	if(xmlHttp==false)
		return;
	
	xmlHttp.onreadystatechange=function()
	{
		if ((xmlHttp.readyState==4 || xmlHttp.readyState=="complete"))
		{ 
			if(xmlHttp.status==200)
			{
				try{
					callbackfun(xmlHttp.responseText);
				} catch (exception) {
					alert(exception);
				}
			}
			else
			{
				alert("Error:"+xmlHttp.status+" "+xmlHttp.statusText+"\nUrl:"+url);
			}
		}
	} 
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
	xmlHttp.send(param);
}

/*跨域Ajax调用服务接口*/
function AjaxCallServer2(url, param) {
	var script = document.getElementById("__SOPR_SCRIPT_NODE_ID__");
	if (script != null && script != undefined) {
		var head = script.parentNode;
		head.removeChild(script);
		script=null;
	}
	script = document.createElement("script");
	script.type = "text/javascript";
	script.src = url + ((url[url.length - 1] == "?") ? "" : "?") + param;
	script.id = "__SOPR_SCRIPT_NODE_ID__";
	var head = document.getElementsByTagName("head")[0];
	head.insertBefore(script, head.firstChild);
}

function getInputTextById(id){
	var value="";
	var obj=document.getElementById(id);
	if(obj!=null && obj!=undefined)
	{
		if(obj.tagName=="TEXTAREA" || (obj.tagName=="INPUT" && (obj.type=="text" || obj.type=="hidden" || obj.type=="password")))
		{
			value=obj.value.trim();
		}
		if(value!=""){
			value=encodeURIComponent(value);
		}
	}
	return value;
}

function setInputTextById(id,value){
	var obj=document.getElementById(id);
	if(obj==null || obj==undefined)
		return;
	if(obj.tagName=="TEXTAREA" || (obj.tagName=="INPUT" && (obj.type=="text" || obj.type=="hidden" || obj.type=="password")))
	{
		if(value==null || value==undefined || value==""){
			obj.value="";
		}else{
			tempdiv=document.createElement("div");
			tempdiv.innerHTML=value;
			obj.value=tempdiv.textContent || tempdiv.innerText;
		}
	}
}

function draftTableUI(id){
	var table = document.getElementById(id);  
	if(table!=null && table!=undefined){
		var rows = table.getElementsByTagName("tr"); 
		for(var i = 0; i < rows.length; i++){   
			if(i % 2 == 0){
				rows[i].style.backgroundColor = "#EFEFEF";
				rows[i].onmouseover = function (){
				      this.style.background="#d9edf7";
				      return true;
				 }
				rows[i].onmouseout=function (){
				      this.style.backgroundColor="#EFEFEF";
				      return true;
				 }
			}else{
				rows[i].style.backgroundColor = "#FFFFFF";
				rows[i].onmouseover = function (){
				      this.style.background="#d9edf7";
				      return true;
				 }
				rows[i].onmouseout=function (){
				      this.style.backgroundColor="#FFFFFF";
				      return true;
				 }
			}
		}
	}
}

/*
 ***************************************************************************************************************************************
 * 封装输入校验类
 ***************************************************************************************************************************************
 */
function SoprCheck(){};

SoprCheck.isDateString=function(str){
	if(str.length!=0){    
	   var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/;     
	   var r = str.match(reg);     
	   if(r!=null){
        	return true;
        }	
	}
	return false;
}

SoprCheck.isTimeString=function(str) { 
   if(str.length!=0){    
	   var reg = /^((20|21|22|23|[0-1]\d)\:[0-5][0-9])(\:[0-5][0-9])?$/;     
	   var r = str.match(reg);     
	   if(r!=null){
        	return true;
        }	
	}
	return false;
}

SoprCheck.isDateTimeString=function(str){
	if(str.length!=0){    
	   var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;     
	   var r = str.match(reg);     
	   if(r!=null){
        	return true;
        }	
	}
	return false;
}
 
SoprCheck.isDigitString=function(str){
	if(str==null || str=="")
		return false;
	var letters="0123456789";
	for(var i=0;i<str.length;i++){
		if(letters.indexOf(str[i])==-1)
		return false;
	}
	return true;
}

SoprCheck.isFlagDigitString=function(str)
{
	if(str==null || str=="")
		return false;

	var letters="0123456789";
	for(var i=0;i<str.length;i++)
	{
		if(letters.indexOf(str[i])==-1)
		{
			if(i==0 && (str[i]=="-" || str[i]=="+"))
			{
				continue;
			}
			else 
				return false;
		}	
	}
	return true;
}  

SoprCheck.isValidGoodsId=function(str)
{
	if(str==null || str==undefined || str=="" ||str.length!=32){
		return false;
	}
	var letters="0123456789ABCDEF";
	for(var i=0;i<str.length;i++){
		if(letters.indexOf(str[i])==-1)
		return false;
	}
	return true;
} 

/*
 ***************************************************************************************************************************************
 * 类型转换类
 ***************************************************************************************************************************************
 */
function SoprConvert(){};

SoprConvert.timeStampToDateTimeString=function(ts){
	var date = new Date(ts*1000);
	var Y = date.getFullYear() + '-';
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
	var D = date.getDate() + ' ';
	var h = (date.getHours()<10 ? '0'+(date.getHours()) :date.getHours()) + ':';
	var m = (date.getMinutes() <10 ? '0'+(date.getMinutes()) : date.getMinutes()) + ':';
	var s = date.getSeconds()<10 ? '0'+(date.getSeconds()) : date.getSeconds(); 
	return (Y+M+D+h+m+s);
}

/*
 ***************************************************************************************************************************************
 * 字符串处理扩展
 ***************************************************************************************************************************************
 */
String.format = function() {
    if (arguments.length == 0)
        return null;
    var str = arguments[0];
    for ( var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        str = str.replace(re, arguments[i]);
    }
    return str;
}

String.prototype.startWith=function(str){
  var reg=new RegExp("^"+str);    
  return reg.test(this);       
} 
 
String.prototype.endWith=function(str){    
  var reg=new RegExp(str+"$");    
  return reg.test(this);       
} 

String.prototype.trim=function() {
	return this.replace(/(^\s*)|(\s*$)/g, ""); 
} 


