/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var name='', state='';
    //表格
    table();
    //查询
    query();
    //新增
    add();
    //弹窗编辑
    edit();
    //状态，编辑
    action();
    //模块列表初始化
    modularList();

    function modularList(){
        $.ajax('/modular/lists.json', {
            method:'GET'
        }).done(function(data){
            if(data.code === 1){
                $('.box-modularList').append(Mustache.render($('#tpl-modularList').html(), data));
            }else{
                alert('获取模块列表失败');
            }
        }).fail(function(){

        }).always(function(){

        });
    }

    function action(){
        $(document).on('click','[data-action=change-state]', function(){
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $state = $tr.find('[data-state]');
            var id = $tr.find('[data-id]').attr('data-id');
            var name = $tr.find('[data-name]').attr('data-name');
            var state = parseInt( $state.attr('data-state')) == 0 ? 1:0;

            $this.attr('disabled', 'disabled');
            $.ajax('/role',{
                method:'PUT',
                data:{
                    id:id,
                    name:name,
                    state:state
                }
            }).done(function(data){
                if(data && data.code == 1){
                    $state.attr('data-state', state);
                    if(state){
                        $state.find('label').removeClass('label-default').addClass('label-success').text('已启用');
                        $this.text('禁用');
                    }else{
                        $state.find('label').removeClass('label-success').addClass('label-default').text('已禁用');
                        $this.text('启用');
                    }
                }
            }).always(function(data){
                $this.attr('disabled', false);
            });
        });


        $(document).on('click', '[data-action=action-edit]', function(){
            var $tr = $(this).closest('tr');
            var $tr_id = $tr.find('[data-id]');
            var $tr_name = $tr.find('[data-name]')
            var $tr_state = $tr.find('[data-state]');
            var $tr_modularList = $tr.find('[data-modularList]');

            var $modalEdit = $('#modal-edit');
            var $id = $modalEdit.find('[name=id]');
            var $name = $modalEdit.find('[name=name]');
            var $state = $modalEdit.find('[name=state]');

            var $checkbox = $modalEdit.find('.box-modularList').find('input[type=checkbox]');

            $id.val($tr_id.attr('data-id'));
            $name.val($tr_name.attr('data-name'));
            $state.val($tr_state.attr('data-state'));

            var modularList = $tr_modularList.attr('data-modularList').split(',');
            //for(var i = 0; i < $checkbox.length; i++){
            //    $checkbox[i].checked = false;
            //}
            $checkbox.attr('checked', false);
            if(modularList[0] !== ''){
                for(var i = 0; i < modularList.length; i++){
                    //$checkbox.eq(modularList[i]).attr('checked', true);
                    $checkbox.filter('[value='+modularList[i]+']')[0].checked = true;
                }
            }


            $('#modal-edit').modal('show');
        });

        $(document).on('click', '[data-action=action-detail]', function(){
            var $tr = $(this).closest('tr');
            var $tr_id = $tr.find('[data-id]');
            var $tr_name = $tr.find('[data-name]')
            var $tr_state = $tr.find('[data-state]');
            var $tr_modularList = $tr.find('[data-modularList]');

            var $modalEdit = $('#modal-detail');
            var $id = $modalEdit.find('[name=id]');
            var $name = $modalEdit.find('[name=name]');
            var $state = $modalEdit.find('[name=state]');

            var $checkbox = $modalEdit.find('.box-modularList').find('input[type=checkbox]');

            $id.val($tr_id.attr('data-id'));
            $name.val($tr_name.attr('data-name'));
            $state.val($tr_state.attr('data-state'));

            var modularList = $tr_modularList.attr('data-modularList').split(',');

            //
            //for(var i = 0; i < $checkbox.length; i++){
            //    $checkbox[i].checked = false;
            //}
            $checkbox.attr('checked', false).attr('disabled', true);
            if(modularList[0] !== ''){
                for(var i = 0; i < modularList.length; i++){
                    //$checkbox.eq(modularList[i]).attr('checked', true);
                    $checkbox.filter('[value='+modularList[i]+']')[0].checked = true;
                }
            }



            $('#modal-detail').modal('show');
        });
    }


    function query(){
        var $form = $('#form-query');
        $('#form-query-btn').click(function(){
            name = $form.find('[name=name]').val();
            state = $form.find('[name=state]').val();
            table();
        });
    }

    function add(){
        var $modalAdd = $('#modal-add');
        var $submit = $modalAdd.find('[data-action=modal-submit]');
        var $name = $modalAdd.find('[name=name]');
        var $state = $modalAdd.find('[name=state]');
        $submit.click(function(){
            var name = $name.val(), state = $state.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/role', {
                method:'POST',
                data:{
                    name:name,
                    state:state
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalAdd.modal('hide');
                    table();
                }else{
                    alert("添加失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function edit(){
        var $modalEdit = $('#modal-edit');
        var $submit = $modalEdit.find('[data-action=modal-submit]');
        var $id = $modalEdit.find('[name=id]');
        var $name = $modalEdit.find('[name=name]');
        var $state = $modalEdit.find('[name=state]');

        $submit.click(function(){
            var $checkbox = $modalEdit.find('.box-modularList').find('input[type=checkbox]');//因为这部分数据是异步加载，所以放到这里面来
            var id = $id.val(), state = $state.val(), name=$name.val();
            var modularList = [];
            for(var i = 0; i < $checkbox.length; i++){
                if($checkbox[i].checked){
                    modularList.push(parseInt($checkbox.eq(i).val()));
                }
            }
            $submit.attr('disabled', 'disabled');
            $.ajax('/role', {
                method:'PUT',
                data:{
                    id:id,
                    name:name,
                    state:state,
                    modularList:JSON.stringify(modularList)
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalEdit.modal('hide');
                    table();
                }else{
                    alert("编辑失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function table(){
        var url = '/role/lists.json?name='+name+"&state="+state;
        var $loading = $('#loading');
        var $tbody = $('#box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                for (var i = 0; i < data.rows.length; i++) {
                    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                }
                $tbody.append(Mustache.render($('#tpl-table').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }
});


