/*
 * JS分页类
 * totalNum：总记录条数
 * totalPage：总页数
 * currentPage：当前页码，下标从0开始，但是显示时从1开始
 * pageSize：分页大小
 * funname:页面切换事件函数，函数原型:fun(topagenum,pagesize)
 */
function SoprPage(totalNum, totalPage, currentPage, pageSize,funname) {
	this.status=false;
	if(totalNum<0 || parseInt(totalNum)!=totalNum){
		alert("ERROR: totalNum invalid");return;
	}
	if(totalPage<0 || parseInt(totalPage)!=totalPage){
		alert("ERROR: totalPage invalid");return;
	}
	if(currentPage<0 || parseInt(currentPage)!=currentPage){
		alert("ERROR: currentPage invalid");return;
	}
	if(pageSize<0 || parseInt(pageSize)!=pageSize){
		alert("ERROR: pageSize invalid");return;
	}
	if(currentPage>totalPage){
		alert("ERROR: currentPage>totalPage");return;
	}
	if(totalNum>totalPage*pageSize){
		alert("ERROR: totalNum>totalPage*pageSize");return;
	}
	if(totalNum<(totalPage-1)*pageSize){
		alert("ERROR: totalNum<(totalPage-1)*pageSize");return;
	}
	if(typeof(eval(funname)) != "function"){
		alert("ERROR: "+funname+" not function");return;
	}
	
	this.status=true;
	this.TotalNum = parseInt(totalNum);
	this.TotalPage = parseInt(totalPage);
	this.CurrentPage = parseInt(currentPage);
	this.PageSize = parseInt(pageSize);
	this.FunName=funname;
	

	this.FormatString = function() {
		if (arguments.length == 0)
			return "";
		var str = arguments[0];
		for (var i = 1; i < arguments.length; i++) {
			var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
			str = str.replace(re, arguments[i]);
		}
		return str;
	} 
	
	this.createPageHtml=function(){
		if(this.status==false){
			return "";
		}
		
		//没有数据或者只有1页时不分页
		if(this.TotalNum==0 || this.TotalPage==0 ){
			return "";
		}
		
		var disbuttonformat="<button style=\"height:26px; width:70px; text-align:center; cursor: pointer\" disabled=\"disabled\" onclick=\"{0}({1},{2})\">{3}</button>";
		var buttonformat="<button style=\"height:26px; width:70px; text-align:center; cursor: pointer\" onclick=\"{0}({1},{2})\">{3}</button>";
		var buttonformats="<button style=\"height:26px;text-align:center; cursor: pointer\" onclick=\"{0}({1},{2})\">{3}</button>";
		
		var htmlstr="<table width=\"100%\" height=\"30px\"><tr>";
		htmlstr+=this.FormatString("<td align=\"left\">共<font color=\"red\">{0}</font>条,每页<font color=\"red\">{1}</font>条,共<font color=\"red\">{2}</font>页,第<font color=\"red\">{3}</font>页</td><td align=\"right\">",
				this.TotalNum,this.PageSize,this.TotalPage,this.CurrentPage+1);
		//首页
		if(this.CurrentPage==0){
			htmlstr+=this.FormatString(disbuttonformat,this.FunName,0,this.PageSize,"首&nbsp;页");
		}else{
			htmlstr+=this.FormatString(buttonformat,this.FunName,0,this.PageSize,"首&nbsp;页");
		}
		//上一页
		if(this.CurrentPage>0){
			htmlstr+=this.FormatString(buttonformat,this.FunName,this.CurrentPage-1,this.PageSize,"上一页");
		}else{
			htmlstr+=this.FormatString(disbuttonformat,this.FunName,this.CurrentPage-1,this.PageSize,"上一页");
		}
		//链接页
		if(this.CurrentPage-3>=0){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage-3,this.PageSize,this.CurrentPage-2);
		}
		if(this.CurrentPage-2>=0){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage-2,this.PageSize,this.CurrentPage-1);
		}
		if(this.CurrentPage-1>=0){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage-1,this.PageSize,this.CurrentPage);
		}
		htmlstr+="<span style=\"color:red;min-width:30px;text-align:center;display:inline-block;font-weight:bold;\">"+(this.CurrentPage+1)+"</span>";
		if(this.CurrentPage+1<this.TotalPage){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage+1,this.PageSize,this.CurrentPage+2);
		}	
		if(this.CurrentPage+2<this.TotalPage){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage+2,this.PageSize,this.CurrentPage+3);
		}
		if(this.CurrentPage+3<this.TotalPage){
			htmlstr+=this.FormatString(buttonformats,this.FunName,this.CurrentPage+3,this.PageSize,this.CurrentPage+4);
		}
		
		//下一页
		if(this.CurrentPage<(this.TotalPage-1)){
			htmlstr+=this.FormatString(buttonformat,this.FunName,this.CurrentPage+1,this.PageSize,"下一页");
		}else{
			htmlstr+=this.FormatString(disbuttonformat,this.FunName,this.CurrentPage+1,this.PageSize,"下一页");
		}
		//末页
		if(this.CurrentPage==(this.TotalPage-1)){
			htmlstr+=this.FormatString(disbuttonformat,this.FunName,this.TotalPage-1,this.PageSize,"末&nbsp;页");
		}else{
			htmlstr+=this.FormatString(buttonformat,this.FunName,this.TotalPage-1,this.PageSize,"末&nbsp;页");
		}
		//跳转页
		var selectstr=this.FormatString("跳至<select style=\"width:70px;height:26px\" onchange=\"{0}({1},{2})\">",this.FunName,'this.value',this.PageSize);
		for(var i=0;i<this.TotalPage;i++){
			if(i==this.CurrentPage){
				selectstr+=this.FormatString("<option value=\"{0}\" selected=\"selected\">{1}</option>",i,i+1);
			}else{
			selectstr+=this.FormatString("<option value=\"{0}\">{1}</option>",i,i+1);
			}
		}
		selectstr+="</select>页";
		htmlstr+=selectstr;
		htmlstr+="<td></tr></table>";
		return htmlstr;
	}
};

