/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var keyword=$('[name=keyword]').val();
    //查询
    keyword_query();
    keyword_add();
    keyword_edit();
    keyword_delete();
    keyword_choose();
    keyword_save();
    keyword_publish();


    function keyword_save(){
        $('[data-action=keyword-save]').click(function(){
            var rows = getKeywordRows();
            var contentArr = [];
            var classIdObj = {};
            for(var i = 0, len = rows.length; i < len; i++){
                var classId = rows[i].classId, classPoint = rows[i].classPoint;
                if(!$.isNumeric(classId) || !$.isNumeric(classPoint)){
                    alert("您有非法数据，请核对后提交！"+classId);
                    return
                }
                if(classId in classIdObj){
                    alert("您有重复数据，请核对后再提交: "+classId);
                    return;
                }else{
                    classIdObj[classId] = true;
                }

                contentArr.push(classId+':'+rows[i].classPoint);
            }
            var content = contentArr.join('-'),
                avaidatetime =  $('[name=avaidatetime]').val()  + ' 23:59:59';
            console.log('avaidatetime: ',avaidatetime);
            $.ajax('/index.php/Search/HotClass/saveUserHotClass', {
                method:"GET",
                data:{
                    keyword:keyword,
                    avaidatetime:avaidatetime,
                    content:content
                }
            }).done(function(data){
                if(data.result){
                    alert('已保存');
                }else{
                    alert("保存出错！"+data.errmsg);
                }
            });
        });
    }

    function keyword_publish(){
        $('[data-action=keyword-publish]').click(function(){
            $.ajax('/index.php/Search/HotClass/pubUserHotClass', {
                method:"POST"
            }).done(function(data){
                if(data.result){
                    alert("发布成功");
                }else{
                    alert("发布失败："+data.errmsg);
                }
            });
        });
    }

    function getKeywordRows(){
        var $tr = $('#hotclass-rg').find('tbody tr');
        var rows = [];
        for(var i = 0; i < $tr.length; i++){
            var $currTr = $tr.eq(i);
            rows.push({
                //keyword:keyword,
                classId:parseInt($currTr.find('[data-classId]').text()),
                //className:$currTr.find('[data-className]').text(),
                classPoint:parseInt($currTr.find('[data-classPoint]').text())
            });
        }

        return rows;
    }

    function keyword_choose(){
        $(document).on('click', '[data-action=action-choose]', function(){
            var $this = $(this);
            var $tr = $this.closest('tr');
            if(this.checked){
                //$this.attr('disabled', 'disabled')
                var data = {
                    keyName:keyword,
                    classId:$tr.find('[data-classId]').attr('data-classId'),
                    className:$tr.find('[data-className]').attr('data-className'),
                    classPoint:$tr.find('[data-classPoint]').attr('data-classPoint')
                }
                insert(data);
            }
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }


    function keyword_delete(){
        var $hotclassAuto = $('#hotclass-auto');
        $(document).on('click', '[data-action=action-delete]', function(){
            var $this = $(this);
            var $tr = $this.closest('tr');
            $tr.remove();
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }



    function keyword_edit(){
        $(document).on('dblclick', '.box-dblclick', function(){
            var $this = $(this);
            var $label = $this.find('.label-dblclick');
            var $input = $this.find('.input-dblclick');
            var val = $label.text();
            $label.hide();
            $input.show().focus().val(val);
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }

    function keyword_add(data){
        $('[data-action=action-add]').click(function(){
            if(!keyword){
                alert("您还没有查询关键词");
                return;
            }
            insert(data);
        });
    }

    function insert(data){
        var defaults = {
            keyName:keyword,
            classId:'',
            className:'自动填充，无需填写',
            classPoint:''
        };

        var $tbody = $('#hotclass-rg').find('.box-tbody');
        var data = $.extend({}, defaults, data);
        console.log('data: ', data);
        $tbody.prepend(Mustache.render($('#tpl-rg-tr').html(), data));
    }

    function keyword_query(){
        var $query = $('#form-query').find('[name=keyword]');
        $('#form-query-btn').click(function(){
            keyword = $query.val();
            table_auto();
            table_rg();
        });
    }

    function table_auto(){
        var hotclassType = 1;
        var $hotclass = $('#hotclass-auto');
        var url = '/index.php/Search/HotClass/getSysHotClass?keyword='+encodeURIComponent(keyword);
        var $loading = $hotclass.find('.loading');
        var $tbody = $hotclass.find('.box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.result) {
                //for (var i = 0; i < data.rows.length; i++) {
                //    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                //}
                $tbody.append(Mustache.render($('#tpl-auto').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }

    function table_rg(){
        var hotclassType = 2;
        var $hotclass = $('#hotclass-rg');
        var url = '/index.php/Search/HotClass/getUserHotClass?keyword='+encodeURIComponent(keyword);
        var $loading = $hotclass.find('.loading');
        var $tbody = $hotclass.find('.box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.result) {
                if(data.data.length > 0){
                    data.isPub = parseInt(data.data[0].isPub);
                    if(data.isPub){
                        $('[name=keyword-state]').val('已发布');
                    }else{
                        $('[name=keyword-state]').val('未发布');
                    }

                    $('[name=avaidatetime]').datepicker('setDate', data.data[0].avaiDateTime.split(' ')[0]);
                }else{
                    //重置日期和状态
                    $('[name=keyword-state]').val('未发布');
                    var endDate = new Date();
                    endDate.setDate(endDate.getDate() + 90);
                    $('[name=avaidatetime]').datepicker('setDate', Yi.formatDate(endDate));
                }
                var html = $('#tpl-rg').html();
                $tbody.append(Mustache.render(html, data));
            }
        }).always(function(){
            $loading.hide();
        });
    }
})