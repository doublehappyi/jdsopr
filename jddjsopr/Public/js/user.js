/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function () {
    //定义一些全局变量
    var username = '', roleId = '', state = '', page = 1;
    //获取角色
    role();
    //表格
    table();
    //查询
    query();
    //新增
    add();
    //弹窗编辑
    edit();
    //状态，
    action_state();
    //编辑，
    action_edit();
    //重置密码
    action_password();

    function action_state() {
        $(document).on('click', '[data-action=change-state]', function () {
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $state = $tr.find('[data-state]');
            var id = $tr.find('[data-id]').attr('data-id');
            var state = parseInt($state.attr('data-state')) == 0 ? 1 : 0;
            var roleId = $tr.find('[data-roleId]').attr('data-roleId');

            $this.attr('disabled', 'disabled');
            $.ajax('/user', {
                method: 'PUT',
                data: {
                    id: id,
                    roleId: roleId,
                    state: state
                }
            }).done(function (data) {
                if (data && data.code == 1) {
                    $state.attr('data-state', state);
                    if (state) {
                        $state.find('label').removeClass('label-default').addClass('label-success').text('已启用');
                        $this.text('禁用');
                    } else {
                        $state.find('label').removeClass('label-success').addClass('label-default').text('已禁用');
                        $this.text('启用');
                    }
                }
            }).always(function (data) {
                $this.attr('disabled', false);
            });
        });
    }

    function action_edit() {
        $(document).on('click', '[data-action=user-edit]', function () {
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $tr_id = $tr.find('[data-id]');
            var $tr_username = $tr.find('[data-username]')
            var $tr_roleId = $tr.find('[data-roleId]');
            var $tr_state = $tr.find('[data-state]');


            var $modalEdit = $('#modal-edit');
            var $id = $modalEdit.find('[name=id]');
            var $username = $modalEdit.find('[name=username]');
            var $roleId = $modalEdit.find('[name=roleId]');
            var $state = $modalEdit.find('[name=state]');

            $id.val($tr_id.attr('data-id'));
            $username.val($tr_username.attr('data-username'));
            $roleId.val($tr_roleId.attr('data-roleId'));
            $state.val($tr_state.attr('data-state'));

            $('#modal-edit').modal('show');
        });
    }

    function action_password() {
        $(document).on('click', '[data-action=reset-password]', function () {
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $id = $tr.find('[data-id]');
            var id = $id.attr('data-id');
            $.ajax('/user/password', {
                method: 'PUT',
                data: {
                    id: id
                }
            }).done(function (data) {
                $('#modal-tip').modal('show');
            }).always(function (data) {
                $this.attr('disabled', false);
            });
        });
    }


    function query() {
        var $form = $('#form-query');
        $('#form-query-btn').click(function () {
            username = $form.find('[name=username]').val();
            roleId = $form.find('[name=roleId]').val();
            state = $form.find('[name=state]').val();
            page = 1;
            table();
        });
    }

    function add() {
        var $modalAdd = $('#modal-add');
        var $submit = $modalAdd.find('[data-action=modal-submit]');
        var $username = $modalAdd.find('[name=username]');
        var $roleId = $modalAdd.find('[name=roleId]');
        var $state = $modalAdd.find('[name=state]');
        $submit.click(function () {
            var username = $username.val(), roleId = $roleId.val(), state = $state.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/user', {
                method: 'POST',
                data: {
                    username: username,
                    roleId: roleId,
                    state: state
                }
            }).done(function (data) {
                if (data.code === 1) {
                    $modalAdd.modal('hide');
                    table();
                } else {
                    alert("添加失败: " + data.msg);
                }
            }).always(function () {
                $submit.attr('disabled', false);
            });
        });
    }

    function edit() {
        var $modalEdit = $('#modal-edit');
        var $submit = $modalEdit.find('[data-action=modal-submit]');
        var $id = $modalEdit.find('[name=id]');
        var $roleId = $modalEdit.find('[name=roleId]');
        var $state = $modalEdit.find('[name=state]');

        $submit.click(function () {
            var id = $id.val(), roleId = $roleId.val(), state = $state.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/user', {
                method: 'PUT',
                data: {
                    id: id,
                    roleId: roleId,
                    state: state
                }
            }).done(function (data) {
                if (data.code === 1) {
                    $modalEdit.modal('hide');
                    table();
                } else {
                    alert("编辑失败: " + data.msg);
                }
            }).always(function () {
                $submit.attr('disabled', false);
            });
        });
    }

    function table() {
        var url = '/user/lists.json?username=' + username + '&roleId=' + roleId + "&state=" + state + "&page=" + page;
        var $loading = $('#loading');
        var $tbody = $('#box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                for (var i = 0; i < data.rows.length; i++) {
                    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                }
                $tbody.append(Mustache.render($('#tpl-table').html(), data));
            }
        }).always(function () {
            $loading.hide();
        });
    }

    function role() {
        $.ajax('/role/lists.json', {
            method: "GET"
        }).done(function (data) {
            if (data && data.code === 1) {
                var html = Mustache.render($('#tpl-role').html(), data);
                $('.box-role').append(html);
            } else {
                alert('获取角色列表失败！');
            }
        });
    }
});


