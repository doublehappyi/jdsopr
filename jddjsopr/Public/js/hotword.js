/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var intervention='', keywords='', store_id='', org_code='', page = 0;
    //表格
    table(0);
    //查询
    query();
    //新增
    add();
    //弹窗编辑
    edit();
    //改变顺序
    sortNumEdit();
    //工具栏按钮
    toolbar();
    //翻页
    pagination();

    function pagination(){
        $(document).on('click', '[data-page]', function(){
            var $this = $(this);
            //由于
            var page = parseInt($this.attr('data-page')) - 1 ;
            table(page);
        });

        $(document).on('change', '[data-action=pagination-select]', function(){
            var $this = $(this);
            var page = parseInt($this.val()) - 1 ;
            table(page);
        });
    }

    function toolbar(){
        //改为屏蔽热词
        $('[data-action=change-intervention-2]').click(function(){
            var $checkbox = $('.keywords-checkbox');
            var ids = [];
            for(var i = 0; i < $checkbox.length; i++){
                if($checkbox[i].checked){
                    ids.push(parseInt($checkbox.eq(i).attr('data-id')));
                }
            }
            if(ids.length === 0){
                alert('请选择热词');
                return;
            }
            $.ajax('/index.php/Search/HotWords/filterHotWords', {
                method:'POST',
                data:{
                    ids:ids.join(',')
                }
            }).done(function(data){
                if(data.result){
                    alert('修改成功');
                    table();
                }else{
                    alert('修改失败：'+data.errmsg);
                }
            }).fail(function(err){
                alert("修改失败"+err);
            });
        });

        //删除热词
        $('[data-action=delete-words]').click(function(){
            var $checkbox = $('.keywords-checkbox');
            var ids = [];
            for(var i = 0; i < $checkbox.length; i++){
                if($checkbox[i].checked){
                    ids.push(parseInt($checkbox.eq(i).attr('data-id')));
                }
            }
            if(ids.length === 0){
                alert('请选择热词');
                return;
            }
            $.ajax('/index.php/Search/HotWords/delHotWords', {
                method:'POST',
                data:{
                    ids:ids.join(',')
                }
            }).done(function(data){
                if(data.result){
                    alert('删除成功');
                    table();
                }else{
                    alert('删除失败：'+data.errmsg);
                }
            }).fail(function(err){
                alert("删除失败"+err);
            });

        });

        //发布
        $('[data-action=keywords-publish]').click(function(){
            $.ajax('/index.php/Search/HotWords/pubHotWords', {
                method:'POST'
            }).done(function(data){
                if(data && data.result){
                    alert('发布成功');
                    table();
                }else{
                    alert('发布失败：'+data.errmsg);
                }
            }).fail(function(err){
                alert("发布成功"+err);
            });

        });

    }

    function sortNumEdit(){
        $(document).on( 'dblclick', '[data-sort_num]', function(e){
            var $this = $(this);
            var $sortNum = $this.find('.sort_num');
            var $sortText = $this.find('.sort_text');

            $sortNum.hide();
            $sortText.show().focus();
        });

        $(document).on( 'blur', '[data-sort_num]', function(e){
            var $this = $(this);
            var $td = $this.closest('td');
            var $sortNum = $td.find('.sort_num');
            var $sortText = $td.find('.sort_text');
            $sortText.attr('disabled', true);
            $.ajax('/index.php/Search/HotWords/changeSortNum',{
                method:'POST',
                data:{
                    sortnum:$sortText.val(),
                    id:$td.attr('data-id')
                }
            }).done(function(data){
                if(data && data.result){
                    //$sortText.hide();
                    //$sortNum.text($sortText.val()).show();
                    table();
                }else{
                    alert('更新失败：'+data.errmsg);
                }
            }).fail(function(err){
                alert('更新失败，'+err);
            }).always(function(){
                $sortText.attr('disabled', false);
            });
        });
    }




    function query(){
        var $form = $('#form-query');
        $('#form-query-btn').click(function(){
            intervention = $form.find('[name=intervention]').val();
            keywords = $form.find('[name=keywords]').val();
            org_code = $form.find('[name=org_code]').val();
            store_id = $form.find('[name=store_id]').val();
            page = 0;
            table();
        });
    }

    function add(){
        var $modalAdd = $('#modal-add');
        var $submit = $modalAdd.find('[data-action=modal-submit]');
        var $keywords = $modalAdd.find('[name=keywords]');
        var $store_id = $modalAdd.find('[name=store_id]');
        var $intervention = $modalAdd.find('[name=intervention]');
        var $startTime = $modalAdd.find('[name=startTime]');
        var $endTime = $modalAdd.find('[name=endTime]');
        $submit.click(function(){
            var keywords = $keywords.val(), store_id = $store_id.val(), intervention = $intervention.val(),
                startTime = $startTime.val(), endTime=$endTime.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/index.php/Search/HotWords/saveUserHotWords', {
                method:'POST',
                data:{
                    keyword:keywords,
                    storeids:store_id,
                    intervention:intervention,
                    starttime:startTime + ' 00:00:00',
                    endtime:endTime + ' 23:59:59'
                }
            }).done(function(data){
                if(data.result){
                    $modalAdd.modal('hide');
                    table();
                }else{
                    alert("添加失败: "+data.errmsg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function edit(){
        var $modalEdit = $('#modal-edit');
        var $submit = $modalEdit.find('[data-action=modal-submit]');
        var $id = $modalEdit.find('[name=id]');
        var $store_id = $modalEdit.find('[name=store_id]');
        var $org_code = $modalEdit.find('[name=org_code]');

        $submit.click(function(){
            var id = $id.val(), store_id = $store_id.val(), org_code = $org_code.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/hotword', {
                method:'PUT',
                data:{
                    id:id,
                    store_id:store_id,
                    org_code:org_code
                }
            }).done(function(data){
                if(data.result){
                    $modalEdit.modal('hide');
                    table();
                }else{
                    alert("编辑失败: "+data.errmsg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function table(page){
        if(page !== 0){
            var page = page || (parseInt($('[data-CurrentPage]').attr('data-CurrentPage')) - 1);
        }
        var url = '/index.php/Search/HotWords/getHotWords?intervention='+intervention+'&keyword='+keywords+'&storeid='+store_id+"&orgcode="+org_code+"&page="+page;
        var $loading = $('#loading');
        var $tbody = $('#box-tbody');
        var $pagination = $('#box-pagination');
        $tbody.empty();
        $pagination.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.result) {
                data.rows = data.data;
                for(var i = 0; i < data.rows.length; i++){
                    var intervention = data.rows[i].intervention;
                    if(intervention == 0){
                        data.rows[i]['isXitong'] = true;
                    }else if(intervention == 1){
                        data.rows[i]['isRengong'] = true;
                    }else if(intervention == 2){
                        data.rows[i]['isPingbi'] = true;
                    }
                }
                //由于后台翻页是从0开始的，所以这里要做加工处理
                data.pageinfo.CurrentPage += 1;

                data.pageinfo.NextPage = data.pageinfo.CurrentPage + 1;
                data.pageinfo.PrevPage = data.pageinfo.CurrentPage - 1;
                data.pageinfo.LastPage = data.pageinfo.TotalPage + 1;
                data.pageinfo.FirstPage = data.pageinfo.FirstPage + 1;
                data.pageinfo.pages = [];

                for(var i = 0; i < data.pageinfo.TotalPage;i++){
                    data.pageinfo.pages.push(i+1);
                }
                if(data.pageinfo.NextPage > data.pageinfo.TotalPage){
                    data.pageinfo.NextPage = false;
                }
                if(data.pageinfo.PrevPage < 1){
                    data.pageinfo.PrevPage = false;
                }
                $tbody.append(Mustache.render($('#tpl-table').html(), data));
                $pagination.append(Mustache.render($('#tpl-pagination').html(), data.pageinfo));
                $('[data-action=pagination-select]').val(data.pageinfo.CurrentPage);
            }
        }).always(function(){
            $loading.hide();
        });
    }
});


